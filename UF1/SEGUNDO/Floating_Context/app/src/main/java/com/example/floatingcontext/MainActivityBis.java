package com.example.floatingcontext;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import java.util.List;

public class MainActivityBis extends AppCompatActivity {

    ArrayList<String> cursos = new ArrayList<>();
    private ListView listView;
    ArrayAdapter<String> arrayAdapter;

    private ActionMode actionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bis);
        registerForContextMenu(listView);

        cursos.add("1");
        cursos.add("2");
        cursos.add("3");
        cursos.add("4");
        cursos.add("5");
        cursos.add("6");
        cursos.add("7");
        cursos.add("8");
        cursos.add("9");
        cursos.add("10");
        cursos.add("11");
        cursos.add("12");
        cursos.add("13");
        cursos.add("14");
        cursos.add("15");
        cursos.add("16");
        cursos.add("17");
        cursos.add("18");
        cursos.add("19");
        cursos.add("20");

        listView = findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<String>(
                this,
                R.layout.support_simple_spinner_dropdown_item, cursos
        );
        listView.setAdapter(arrayAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (actionMode != null) {
                    return false;
                }

                actionMode = startSupportActionMode(actionModeCallback);
                return true;
            }
        });

    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.my_menu, menu);
            mode.setTitle("Choose an option:");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.item1:
                    Toast.makeText(MainActivityBis.this, "Delete", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.item2:
                    Toast.makeText(MainActivityBis.this, "Share", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode=null;
        }
    };

}