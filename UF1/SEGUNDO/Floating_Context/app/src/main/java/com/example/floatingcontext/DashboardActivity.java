package com.example.floatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class DashboardActivity extends AppCompatActivity {


    private Button btListView;
    private Button btGridViewText;
    private Button btGridViewImages;
    private Button btListViewImages;
    private Button btListViewClass;
    private Button button;
    private Button btbis;
    private Button btcheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        btListView = findViewById(R.id.btListView);
        btGridViewText = findViewById(R.id.btGridViewText);
        btGridViewImages = findViewById(R.id.btGridViewImages);
        btListViewImages = findViewById(R.id.btListViewImages);
        btListViewClass = findViewById(R.id.btListViewClass);
        button = findViewById(R.id.button);
        btbis = findViewById(R.id.btbis);
        btcheck = findViewById(R.id.btcheck);


        btListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btListViewImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, MainActivity5.class);
                startActivity(intent);
            }
        });




       btGridViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, MainActivity4.class);
                startActivity(intent);
            }
        });

        btGridViewImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });




       btListViewClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (DashboardActivity.this, MainActivity3.class);
                startActivity(intent);
            }
        });


       button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (DashboardActivity.this, MainActivity6.class);
                startActivity(intent);
            }
        });





        btbis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (DashboardActivity.this, MainActivityBis.class);
                startActivity(intent);
            }
        });

        btcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (DashboardActivity.this, MainActivityCheck.class);
                startActivity(intent);
            }
        });


    }
}


