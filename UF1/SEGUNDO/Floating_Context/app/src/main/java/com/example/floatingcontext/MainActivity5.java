package com.example.floatingcontext;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity5 extends AppCompatActivity {

    ListView listView;

    private static Integer[] imagesIDs = {
            R.drawable.imagen1,
            R.drawable.imagen2,
            R.drawable.imagen3,
            R.drawable.imagen4,
            R.drawable.imagen5,
            R.drawable.imagen6,
    };

    static String[] imageText = {"imagen1", "imagen2", "imagen3", "imagen4", "imagen5", "imagen6"};

    CustomAdapter customAdapter = new CustomAdapter();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        listView = findViewById(R.id.listView);

        listView.setAdapter(customAdapter);

        registerForContextMenu(listView);

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Escoge una opción:");
        menu.add(0, 1, 1, "Share");
        menu.add(0, 2, 2, "Delete");
        menu.add(0, 3, 3, "Web");


    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT);
                return true;
            case 2:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT);
                return true;
            case 3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return imagesIDs.length;
        }

        @Override
        public Object getItem(int position) {
            return imagesIDs[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //inflar amb el layout
            View view = getLayoutInflater().inflate(R.layout.row_data, null);

            //hooks
            TextView textRow = view.findViewById(R.id.textRow);
            ImageView imageRow = view.findViewById(R.id.imageRow);

            //set's --> text y images
            textRow.setText(imageText[position]);
            imageRow.setImageResource(imagesIDs[position]);

            return view;
        }
    }
}