package com.example.floatingcontext;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class ActivityDetail extends AppCompatActivity {

    private TextView textRow;
    private TextView textDesc;
    private TextView textSubtitle;
    private TextView textFrom;
    private ImageView imageBig;
    private ImageView imageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textRow = findViewById(R.id.textRow);
        textDesc = findViewById(R.id.textDesc);
        textFrom = findViewById(R.id.textFrom);
        textSubtitle = findViewById(R.id.textSubtitle);
        imageBig = findViewById(R.id.imageBig);
        imageName = findViewById(R.id.imageName);


        Intent intent = getIntent();
        String text_Row = intent.getStringExtra(MainActivity3.EXTRA_TEXT_TITLE);
        String text_Desc = intent.getStringExtra(MainActivity3.EXTRA_TEXT_DESC);
        String text_From = intent.getStringExtra(MainActivity3.EXTRA_TEXT_FROM);
        String text_Subtitle = intent.getStringExtra(MainActivity3.EXTRA_TEXT_SUBTITLE);
        int image_Name = intent.getIntExtra(MainActivity3.EXTRA_IMAGE, 1);
        int image_Big = intent.getIntExtra(MainActivity3.EXTRA_IMAGE_BIG, 1);

        textRow.setText(text_Row);
        textDesc.setText(text_Desc);
        textFrom.setText(text_From);
        textSubtitle.setText(text_Subtitle);
        imageName.setImageResource(image_Name);
        imageBig.setImageResource(image_Big);
    }





}
