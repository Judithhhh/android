package com.example.floatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity4 extends AppCompatActivity {

    private GridView gridView;

    List<String> cursos = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        gridView = findViewById(R.id.gridView);

        cursos.add ("1");
        cursos.add ("2");
        cursos.add ("3");
        cursos.add ("4");
        cursos.add ("5");
        cursos.add ("6");
        cursos.add ("7");
        cursos.add ("8");
        cursos.add ("9");
        cursos.add ("10");

        dataAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, cursos);

        gridView.setAdapter(dataAdapter);
        registerForContextMenu(gridView);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Escoge una opción:");
        menu.add(0, 1, 1, "Share");
        menu.add(0, 2, 2, "Delete");
        menu.add(0, 3, 3, "Web");

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT);
                return true;
            case 2:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT);
                return true;
            case 3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}