package com.example.floatingcontext;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivityCheck extends AppCompatActivity {

    ArrayList<String> cursos = new ArrayList<>();
    private ListView listView;
    ArrayAdapter<String> arrayAdapter;

    private ActionMode actionMode;

    ArrayList<String> checkedItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_check);
        registerForContextMenu(listView);
/*
        cursos.add("1");
        cursos.add("2");
        cursos.add("3");
        cursos.add("4");
        cursos.add("5");
        cursos.add("6");
        cursos.add("7");
        cursos.add("8");
        cursos.add("9");
        cursos.add("10");
        cursos.add("11");
        cursos.add("12");
        cursos.add("13");
        cursos.add("14");
        cursos.add("15");
        cursos.add("16");
        cursos.add("17");
        cursos.add("18");
        cursos.add("19");
        cursos.add("20");

        listView = findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_checked, cursos);

        listView.setAdapter(arrayAdapter);

        final AbsListView.MultiChoiceModeListener choiceListener = new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(android.view.ActionMode mode, int i, long id, boolean b) {
                checkedItems.add(cursos.get(i));
            }


            @Override
            public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                MenuInflater menuInflater = actionMode.getMenuInflater();
                menuInflater.inflate(R.menu.my_menu, menu);
                actionMode.setTitle("Choose an option:");
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                switch (my_menu.getItemId()) {
                    case R.id.item1:
                        Toast.makeText(MainActivityCheck.this, "Delete", Toast.LENGTH_SHORT).show();

                        SparseBooleanArray checked = listView.getCheckedItemPositions();

                        for (String i : checkedItems) {
                            cursos.remove(i);
                        }

                        checkedItems.clear();
                        listView.getCheckedItemPositions().clear();
                        arrayAdapter.notifyDataSetChanged();
                        return true;
                    default:
                        return true;

                }
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode mode) {
                actionMode = null;
            }
        };

        listView.setOnItemLongClickListener();
        
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.my_menu, menu);
            mode.setTitle("Choose an option:");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.item1:
                    Toast.makeText(MainActivityCheck.this, "Delete", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.item2:
                    Toast.makeText(MainActivityCheck.this, "Share", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode=null;
        }
    };


    AbsListView.MultiChoiceModeListener modeListener = new AbsListView.MultiChoiceModeListener() {
        @Override
        public void onItemCheckedStateChanged(android.view.ActionMode mode, int position, long id, boolean checked) {

        }

        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {

            if (actionMode != null){
                return false;
            }

            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.my_menu, menu);
            mode.setTitle("Choose an option:");
            isActiveActionmode = true;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.item1:
                    Toast.makeText(MainActivityCheck.this, "Delete", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.item2:
                    Toast.makeText(MainActivityCheck.this, "Share", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {
            actionMode = null;
            isActiveActionMode = false;
        }
    };

    private class CustumAdapterMulti extends BaseAdapter {
        private Context context;
        private ArrayList<String> cursos;

        public CustumAdapterMulti(Context context, ArrayList<String> cursos) {
            this.context = context;
            this.cursos = cursos;
        }

        @Override
        public int getCount() {
            return cursos.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data_multi,null);
            TextView textRow = view.findViewById(R.id.textRow);
            CheckBox checkBox = view.findViewById(R.id.checkBox);

            textRow.setText(cursos.get(position));

            if (isActiveActionMode){
                checkBox.setVisibility(view.VISIBLE);
            }
            return view;
        }
    }


}*/
    }
}
