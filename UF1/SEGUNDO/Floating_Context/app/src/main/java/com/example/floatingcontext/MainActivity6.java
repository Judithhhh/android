package com.example.floatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity6 extends AppCompatActivity {

    public static String EXTRA_TEXT_TITLE = "com.example.startup.EXTRA_TEXT_TITLE ";
    public static String EXTRA_TEXT_DESC = "com.example.startup.EXTRA_TEXT_DESC ";
    public static String EXTRA_TEXT_FROM = "com.example.startup.EXTRA_TEXT_FROM ";
    public static String EXTRA_TEXT_SUBTITLE = "com.example.startup.EXTRA_TEXT_SUBTITLE ";
    public static String EXTRA_IMAGE = "com.example.startup.EXTRA_IMAGE ";
    public static String EXTRA_IMAGE_BIG = "com.example.startup.EXTRA_IMAGE_BIG ";

    ListView listView;

    Item item1 = new Item(" Ejemplo 1", "Subtitulo 1",R.drawable.imagen1, "5$", "Descripción 1", R.drawable.imagen1);
    Item item2 = new Item(" Ejemplo 2", "Subtitulo 2",R.drawable.imagen2, "4$", "Descripción 2", R.drawable.imagen2);
    Item item3 = new Item(" Ejemplo 3", "Subtitulo 3",R.drawable.imagen3, "4.10$", "Descripción 3", R.drawable.imagen3);
    Item item4 = new Item(" Ejemplo 4", "Subtitulo 4",R.drawable.imagen4, "2.30$", "Descripción 4", R.drawable.imagen4);
    Item item5 = new Item(" Ejemplo 5", "Subtitulo 5",R.drawable.imagen5, "1$", "Descripción 5", R.drawable.imagen5);
    Item item6 = new Item(" Ejemplo 6", "Subtitulo 6",R.drawable.imagen6, "5.20$", "Descripción 6", R.drawable.imagen6);

    ArrayList<Item> items = new ArrayList<>();

    CustomAdapter customAdapter = new CustomAdapter(this, items);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        registerForContextMenu(listView);

        listView = findViewById(R.id.listView);
        listView.setAdapter(customAdapter);

        registerForContextMenu(listView);

        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);
        items.add(item5);
        items.add(item6);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Escoge una opción:");
        menu.add(0, 1, 1, "Details");
        menu.add(0, 2, 2, "Delete");
        menu.add(0, 3, 3, "Web");

        Log.d("tag", "Hola");


    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Details" + item.getItemId() + " " + info.id + " " + info.position, Toast.LENGTH_SHORT).show();

                return true;
            case 2:
                Toast.makeText(this, "Delete" + item + " " + info.id + " " + info.position, Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Web" + item + " " + info.id, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }



    class CustomAdapter extends BaseAdapter{

        private Context context;
        private ArrayList<Item> items;


        public CustomAdapter(Context context, ArrayList<Item> item) {
            this.context = context;
            this.items = item;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.activity_row_data4, null);

            TextView textRow = view.findViewById(R.id.textRow);
            TextView textSubtitle= view.findViewById(R.id.textSubtitle);
            TextView textFrom = view.findViewById(R.id.textFrom);
            ImageView imageRow = view.findViewById(R.id.imageRow);

            textRow.setText(items.get(position).getItemName());
            textSubtitle.setText(items.get(position).getItemName());
            textFrom.setText(items.get(position).getItemName());
            imageRow.setImageResource(items.get(position).getImageName());

            return view;
        }
    }



}