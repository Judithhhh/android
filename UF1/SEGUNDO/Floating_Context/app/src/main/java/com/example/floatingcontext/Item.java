package com.example.floatingcontext;

public class Item {
    private String itemName;
    private String itemSubtitle;
    private int imageName;
    private String itemFrom;
    private String itemDesc;
    private int imageBig;

    public Item(String itemName, String itemSubtitle, int imageName, String itemFrom, String itemDesc, int imageBig) {
        this.itemName = itemName;
        this.itemSubtitle = itemSubtitle;
        this.imageName = imageName;
        this.itemFrom = itemFrom;
        this.itemDesc = itemDesc;
        this.imageBig = imageBig;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemSubtitle() {
        return itemSubtitle;
    }

    public void setItemSubtitle(String itemSubtitle) {
        this.itemSubtitle = itemSubtitle;
    }

    public int getImageName() {
        return imageName;
    }

    public void setImageName(int imageName) {
        this.imageName = imageName;
    }

    public String getItemFrom() {
        return itemFrom;
    }

    public void setItemFrom(String itemFrom) {
        this.itemFrom = itemFrom;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public int getImageBig() {
        return imageBig;
    }

    public void setImageBig(int imageBig) {
        this.imageBig = imageBig;
    }
}
