package com.example.examenjudith;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    private TextView textRow2;
    private TextView textRow3;
    private TextView textRow4;
    private TextView textRow5;
    private TextView textRow6;
    private TextView textRow7;
    private ImageView imageRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textRow2 = findViewById(R.id.textRow2);
        textRow3 = findViewById(R.id.textRow3);
        textRow4 = findViewById(R.id.textRow4);
        textRow5 = findViewById(R.id.textRow5);
        textRow6 = findViewById(R.id.textRow6);
        textRow7 = findViewById(R.id.textRow7);
        imageRow = findViewById(R.id.imageRow);

        Intent intent = getIntent();
        String text2 = intent.getStringExtra(MainActivity.EXTRA_TEXT_TRAINER);
        String text3 = intent.getStringExtra(MainActivity.EXTRA_TEXT_NOM_TRAINER);
        String text4 = intent.getStringExtra(MainActivity.EXTRA_TEXT_DATE);
        String text5 = intent.getStringExtra(MainActivity.EXTRA_TEXT_DATEX);
        String text6 = intent.getStringExtra(MainActivity.EXTRA_TEXT_TIME);
        String text7 = intent.getStringExtra(MainActivity.EXTRA_TEXT_TIMEX);
        int image = intent.getIntExtra(MainActivity.EXTRA_IMAGE_NAME, 1);

        textRow2.setText(text2);
        textRow3.setText(text3);
        textRow4.setText(text4);
        textRow5.setText(text5);
        textRow6.setText(text6);
        textRow7.setText(text7);
        imageRow.setImageResource(image);


    }



}