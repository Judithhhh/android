package com.example.examenjudith;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

    public class Item {
        private int imageName;

        private String itemTitle;

        private String itemTrainer;
        private String itemNomTrainer;

        private String itemDate;
        private String itemDateX;
        private String itemTime;
        private String itemTimeX;

        public Item(int imageName, String itemTitle, String itemTrainer, String itemNomTrainer, String itemDate, String itemDateX, String itemTime, String itemTimeX) {
            this.itemTitle = itemTitle;
            this.itemTrainer = itemTrainer;
            this.itemNomTrainer = itemNomTrainer;
            this.imageName = imageName;
            this.itemDate = itemDate;
            this.itemTime = itemTime;
            this.itemDateX = itemDateX;
            this.itemTimeX = itemTimeX;
        }

        public String getItemNomTrainer() {
            return itemNomTrainer;
        }

        public void setItemNomTrainer(String itemNomTrainer) {
            this.itemNomTrainer = itemNomTrainer;
        }

        public String getItemTitle() {
            return itemTitle;
        }

        public void seItemTitle(String itemTitle) {
            this.itemTitle = itemTitle;
        }

        public String getItemTrainer() {
            return itemTrainer;
        }

        public void setItemTrainer(String itemTrainer) {
            this.itemTrainer = itemTrainer;
        }

        public int getImageName() {
            return imageName;
        }

        public void setImageName(int imageName) {
            this.imageName = imageName;
        }

        public String getItemDate() {
            return itemDate;
        }

        public void setItemDate(String itemDate) {
            this.itemDate = itemDate;
        }

        public String getItemTime() {
            return itemTime;
        }

        public void setItemTime(String itemTime) {
            this.itemTime = itemTime;
        }

        public String getItemDateX() {
            return itemDateX;
        }

        public void setItemDateX(String itemDateX) {
            this.itemDateX = itemDateX;
        }

        public String getItemTimeX() {
            return itemTimeX;
        }

        public void setItemTimeX(String itemTimeX) {
            this.itemTimeX = itemTimeX;
        }
    }
