package com.example.examenjudith;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Toolbar myToolbar;
    ListView listView;

    public static String EXTRA_TEXT_TITLE = "com.example.startup.EXTRA_TEXT_TITLE ";
    public static String EXTRA_TEXT_TRAINER = "com.example.startup.EXTRA_TEXT_TRAINER ";
    public static String EXTRA_TEXT_NOM_TRAINER = "com.example.startup.EXTRA_TEXT_NOM_TRAINER ";
    public static String EXTRA_TEXT_DATE = "com.example.startup.EXTRA_TEXT_DATE ";
    public static String EXTRA_TEXT_DATEX = "com.example.startup.EXTRA_TEXT_DATEX";
    public static String EXTRA_TEXT_TIME = "com.example.startup.EXTRA_TEXT_TIME";
    public static String EXTRA_TEXT_TIMEX = "com.example.startup.EXTRA_TEXT_TIMEX ";
    public static String EXTRA_IMAGE_NAME = "com.example.startup.EXTRA_IMAGE_NAME ";


    Item item1 = new Item(R.drawable.spinning0, "Spinning", "Trainer:", "Nom 1", "Date:","10/10/2020", "Time", "xx:xx");
    Item item2 = new Item(R.drawable.fitness0, "Fitness", "Trainer:","Nom 2",  "Date:","11/05/2020", "Time", "xx:xx");
    Item item3 = new Item(R.drawable.kombat0, "Kombat", "Trainer:","Nom 3",  "Date:","05/06/2020", "Time", "xx:xx");
    Item item4 = new Item(R.drawable.spartan0, "Spartan", "Trainer:","Nom 4",  "Date:","20/10/2020", "Time", "xx:xx");
    Item item5 = new Item(R.drawable.zumba0, "Zumba", "Trainer:", "Nom 5", "Date:", "10/03/2020", "Time", "xx:xx");
    Item item6 = new Item(R.drawable.spinning0,  "Spinning","Trainer:","Nom 6",  "Date:","11/10/2020", "Time", "xx:xx");
    Item item7 = new Item(R.drawable.fitness0, "Fitness", "Trainer:","Nom 7",  "Date:","10/08/2020", "Time", "xx:xx");

    ArrayList<Item> items = new ArrayList<>();

    CustomAdapter customAdapter = new CustomAdapter(this, items);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerForContextMenu(listView);

        //hook
        myToolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(myToolbar);

        listView = findViewById(R.id.listView);
        listView.setAdapter(customAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);
        items.add(item5);
        items.add(item6);
        items.add(item7);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemPerson:
                //intents
                Toast.makeText(this, "Person", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShop:
                //intents
                Toast.makeText(this, "Shop", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0, 1, 1, "Reserve");
        menu.add(0, 2, 2, "Info");
        menu.add(0, 3, 3, "Share");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Reserve " + item + " " + item.getItemId() + " " + info.position, Toast.LENGTH_SHORT).show();
                displayInfo(info.position);
                return true;
            case 2:
                Toast.makeText(this, "Info " + item + " " + item.getItemId() , Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Share " + item + " " + item.getItemId(), Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void displayInfo(int position) {
        Intent i = new Intent(getApplicationContext(), MainActivity2.class);
        i.putExtra(EXTRA_TEXT_TRAINER,items.get(position).getItemTrainer());
        i.putExtra(EXTRA_TEXT_NOM_TRAINER,items.get(position).getItemNomTrainer());
        i.putExtra(EXTRA_TEXT_DATE,items.get(position).getItemDate());
        i.putExtra(EXTRA_TEXT_DATEX,items.get(position).getItemDateX());
        i.putExtra(EXTRA_TEXT_TIME,items.get(position).getItemTime());
        i.putExtra(EXTRA_TEXT_TIMEX,items.get(position).getItemTimeX());
        i.putExtra(EXTRA_IMAGE_NAME, items.get(position).getImageName());
        startActivity(i);
    }

    private class CustomAdapter extends BaseAdapter {
        private Context context; //context
        private ArrayList<Item> items; //data source of the list adapter

        //constructor
        public CustomAdapter(Context context, ArrayList<Item> items) {
            this.context = context;
            this.items = items;
        }
        @Override
        public int getCount() {
            return items.size();
        }
        @Override
        public Object getItem(int position) {
            //return null;
            return items.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //return null;
            View view = getLayoutInflater().inflate(R.layout.row_data, null);

            TextView textRow = view.findViewById(R.id.textRow);
            TextView textRow2 = view.findViewById(R.id.textRow2);
            TextView textRow3 = view.findViewById(R.id.textRow3);
            TextView textRow4 = view.findViewById(R.id.textRow4);
            TextView textRow5 = view.findViewById(R.id.textRow5);
            TextView textRow6 = view.findViewById(R.id.textRow6);
            TextView textRow7 = view.findViewById(R.id.textRow7);

            ImageView imageRow = view.findViewById(R.id.imageRow);

            textRow.setText(items.get(position).getItemTitle());
            textRow2.setText(items.get(position).getItemTrainer());
            textRow3.setText(items.get(position).getItemNomTrainer());
            textRow4.setText(items.get(position).getItemDate());
            textRow5.setText(items.get(position).getItemDateX());
            textRow6.setText(items.get(position).getItemTime());
            textRow7.setText(items.get(position).getItemTimeX());
            imageRow.setImageResource(items.get(position).getImageName());
            return view;
        }
    }
}