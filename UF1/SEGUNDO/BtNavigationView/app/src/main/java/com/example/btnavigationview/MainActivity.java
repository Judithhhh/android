package com.example.btnavigationview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView mMainNav;
    private FrameLayout frameLayout;

    private HomeFragment homeFragment;
    private PersonFragment profileFragment;
    private SettingFragment settingsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mMainNav = findViewById(R.id.main_nav);
        frameLayout = findViewById(R.id.main_frame);
        homeFragment = HomeFragment.newInstance("Home1", "Home2");
        profileFragment = PersonFragment.newInstance("Profile1", "Profile2");
        settingsFragment = SettingFragment.newInstance("Setting1", "Settings2");

        /*homeFragment = new BlankFragment();
        profileFragment = new BlankFragment2();
        settingsFragemnt = new BlankFragment3();*/
        displayFragment(homeFragment);

        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        displayFragment(homeFragment);
                        //open the HomeFragment
                        //We can change the color of the background using the following:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        return true;
                    case R.id.nav_profile:
                        displayFragment(profileFragment);
                        //open the ProfileFragment
                        mMainNav.setItemBackgroundResource(R.color.colorPrimary);
                        return true;
                    case R.id.nav_settings:
                        displayFragment(settingsFragment);
                        //open the SettingsFragment
                        mMainNav.setItemBackgroundResource(R.color.colorAccent);
                        return true;
                    default:
                        return false;
                }
            }
        });


    }

    private void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}