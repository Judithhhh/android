package com.example.juegojudith;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MainActivity2 extends AppCompatActivity {

    public static String FILE_SHARED_NAME = "MySharedFile";

    ObjectAnimator objectAnimatorFuntion;
    Button buttonPlay;
    int numeroCard;
    Integer actualImg;
    private int contadorCardsDades;
    private int CANTIDAD_CARD = 35;
    private int contadorBank = 1;
    private int contadorGlobalBank = 0;
    private int contadorGlobalYou = 0;
    private ArrayList<Integer> arrayCardsRandom;

    TextView textYouHaveWon;
    TextView textBankWinner;
    Button buttonOneMore;
    Button buttonStop;
    Button buttonPlayAgain;
    TextView textViewYouScore;
    TextView textViewBankScore;
    TextView youTotalScoreTextView;
    TextView bankTotalScoreTextView;
    private Double bankScoreSum = 0.0;
    private Double youScoreSum = 0.0;

    private ImageView imageCardMove;
    private ImageView imageCartasMazo;


    private ImageView imageCard1Bank;
    private ImageView imageCard2Bank;
    private ImageView imageCard3Bank;
    private ImageView imageCard4Bank;
    private ImageView imageCard5Bank;
    private ImageView imageCard6Bank;
    private ImageView imageCard7Bank;
    private ImageView imageCard8Bank;
    private ImageView imageCard9Bank;
    private ImageView imageCard10Bank;
    private ImageView imageCard11Bank;
    private ImageView imageCard12Bank;
    private ImageView imageCard13Bank;
    private ImageView imageCard14Bank;
    private ImageView imageCard15Bank;

    private ImageView imageCard1You;
    private ImageView imageCard2You;
    private ImageView imageCard3You;
    private ImageView imageCard4You;
    private ImageView imageCard5You;
    private ImageView imageCard6You;
    private ImageView imageCard7You;
    private ImageView imageCard8You;
    private ImageView imageCard9You;
    private ImageView imageCard10You;
    private ImageView imageCard11You;
    private ImageView imageCard12You;
    private ImageView imageCard13You;
    private ImageView imageCard14You;
    private ImageView imageCard15You;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Hook
        imageCartasMazo = findViewById(R.id.imageCartasMazo);
        imageCardMove = findViewById(R.id.imageCardMove);
        buttonPlay = findViewById(R.id.buttonPlay);
        buttonOneMore = findViewById(R.id.buttonOneMore);
        buttonStop = findViewById(R.id.buttonStop);
        arrayCardsRandom = new ArrayList<>();

        imageCard1Bank = findViewById(R.id.imageCard1Bank);
        imageCardMove = findViewById(R.id.imageCardMove);
        imageCard2Bank = findViewById(R.id.imageCard2Bank);
        imageCard3Bank = findViewById(R.id.imageCard3Bank);
        imageCard4Bank = findViewById(R.id.imageCard4Bank);
        imageCard5Bank = findViewById(R.id.imageCard5Bank);
        imageCard6Bank = findViewById(R.id.imageCard6Bank);
        imageCard7Bank = findViewById(R.id.imageCard7Bank);
        imageCard8Bank = findViewById(R.id.imageCard8Bank);
        imageCard9Bank = findViewById(R.id.imageCard9Bank);
        imageCard10Bank = findViewById(R.id.imageCard10Bank);
        imageCard11Bank = findViewById(R.id.imageCard11Bank);
        imageCard12Bank = findViewById(R.id.imageCard12Bank);
        imageCard13Bank = findViewById(R.id.imageCard13Bank);
        imageCard14Bank = findViewById(R.id.imageCard14Bank);
        imageCard15Bank = findViewById(R.id.imageCard15Bank);

        imageCard1You = findViewById(R.id.imageCard1You);
        imageCard2You = findViewById(R.id.imageCard2You);
        imageCard3You = findViewById(R.id.imageCard3You);
        imageCard4You = findViewById(R.id.imageCard4You);
        imageCard5You = findViewById(R.id.imageCard5You);
        imageCard6You = findViewById(R.id.imageCard6You);
        imageCard7You = findViewById(R.id.imageCard7You);
        imageCard8You = findViewById(R.id.imageCard8You);
        imageCard9You = findViewById(R.id.imageCard9You);
        imageCard10You = findViewById(R.id.imageCard10You);
        imageCard11You = findViewById(R.id.imageCard11You);
        imageCard12You = findViewById(R.id.imageCard12You);
        imageCard13You = findViewById(R.id.imageCard13You);
        imageCard14You = findViewById(R.id.imageCard14You);
        imageCard15You = findViewById(R.id.imageCard15You);

        textViewBankScore = findViewById(R.id.textAddScoreBank);
        textViewYouScore = findViewById(R.id.textAddScoreYou);
        youTotalScoreTextView = findViewById(R.id.textAddScoreYou);
        bankTotalScoreTextView = findViewById(R.id.textTotalScoreBank);
        buttonPlayAgain = findViewById(R.id.buttonPlayAgain);
        textYouHaveWon = findViewById(R.id.texYouHaveWon);
        textBankWinner = findViewById(R.id.bankWinner);

        buttonPlayAgain.setEnabled(false);

        int index = 0;
        while (index <= CANTIDAD_CARD) {
            int random = new Random().nextInt((CANTIDAD_CARD) + 1);
            boolean repetido = false;
            while (!repetido) {
                for (int i = 0; i < index; i++) {
                    if (random == arrayCardsRandom.get(i)) {
                        repetido = true;
                        break;
                    }
                }
                if (!repetido) {
                    arrayCardsRandom.add(random);
                    index++;
                }
            }
        }


        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                numeroCard = arrayCardsRandom.get(contadorCardsDades);
                actualImg = CardsGameFuction(numeroCard);
                imageCard1You.setImageResource(actualImg);
                youScoreSum = Double.valueOf(mapCardsValue(actualImg));

                textViewYouScore.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        textViewYouScore.setText(String.valueOf(youScoreSum));
                    }
                }, 2000);

                function(imageCardMove, "x", imageCard1You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard1You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard1You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard1You, "alpha", 0f, 1f, 2000, 500);
                contadorCardsDades++;

                function(buttonPlay, "alpha", 1f, 0f, 0, 100);

                function(buttonOneMore, "alpha", 0f, 1f, 500, 1000);
                function(buttonOneMore, "translationX", -1000f, -10f, 0, 2000);
                function(buttonStop, "alpha", 0f, 1f, 500, 1000);
                function(buttonStop, "translationX", 1000f, 10f, 0, 2000);
                buttonPlay.setEnabled(false);

            }
        });

        read();

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                while (bankScoreSum < 7) {
                    restartImgMove();
                    stopBankFunction();

                    function(buttonOneMore, "alpha", 0f, 1f, 500, 1000);
                    function(buttonOneMore, "translationX", -1000f, -10f, 0, 2000);
                    function(buttonStop, "alpha", 0f, 1f, 500, 1000);
                    function(buttonStop, "translationX", 1000f, 10f, 0, 2000);

                }
                comprobacionGanadorBank();


            }
        });
        buttonPlayAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity2.this, MainActivity2.class);

                startActivity(intent);
            }
        });


        buttonOneMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (youScoreSum < 7.5) {
                    restartImgMove();
                    oneMoreFuction();
                }
                function(buttonOneMore, "alpha", 1f, 0f, 500, 1000);
                function(buttonOneMore, "translationX", -10f, -1000f, 0, 2000);
                function(buttonStop, "alpha", 1f, 0f, 500, 1000);
                function(buttonStop, "translationX", 10f, 1000f, 0, 2000);

                comprobacionGanadorYou();


            }
        });


    }


    public void function(Object objectForAll, String propertyName, float value, int startDelay, int duration) {
        objectAnimatorFuntion = ObjectAnimator.ofFloat(objectForAll, propertyName, value);
        objectAnimatorFuntion.setStartDelay(startDelay);
        objectAnimatorFuntion.setDuration(duration);
        objectAnimatorFuntion.start();
    }


    public void function(Object objectForAll, String propertyName, float valueA, float valueB, int startDelay, int duration) {
        objectAnimatorFuntion = ObjectAnimator.ofFloat(objectForAll, propertyName, valueA, valueB);
        objectAnimatorFuntion.setStartDelay(startDelay);
        objectAnimatorFuntion.setDuration(duration);
        objectAnimatorFuntion.start();
    }

    public void comprobacionGanadorBank() {
        if (bankScoreSum > 7.5) {
            function(buttonOneMore, "alpha", 1f, 0f, 500, 1000);
            function(buttonOneMore, "translationX", -10f, -1000f, 0, 2000);
            function(buttonStop, "alpha", 1f, 0f, 500, 1000);
            function(buttonStop, "translationX", 10f, 1000f, 0, 2000);

            buttonPlayAgain.setEnabled(true);
            function(buttonPlayAgain, "alpha", 0f, 1f, 2000, 1000);
            function(textYouHaveWon, "alpha", 0, 1, 1000, 1000);
            function(textYouHaveWon, "scaleX", 1, 1, 1000, 1000);
            function(textYouHaveWon, "scaleY", 1, 1, 1000, 1000);
            youWinner();
        }
        if (bankScoreSum == 7.5) {
            function(buttonOneMore, "alpha", 1f, 0f, 500, 1000);
            function(buttonOneMore, "translationX", -10f, -1000f, 0, 2000);
            function(buttonStop, "alpha", 1f, 0f, 500, 1000);
            function(buttonStop, "translationX", 10f, 1000f, 0, 2000);

            buttonPlayAgain.setEnabled(true);
            function(buttonPlayAgain, "alpha", 0f, 1f, 2000, 1000);
            function(textBankWinner, "alpha", 0, 1, 1000, 1000);
            function(textBankWinner, "scaleX", 1, 1, 1000, 1000);
            function(textBankWinner, "scaleY", 1, 1, 1000, 1000);
            bankWinner();
        }

        if (bankScoreSum <= 7 && bankScoreSum >= youScoreSum) {
            function(buttonOneMore, "alpha", 1f, 0f, 500, 1000);
            function(buttonOneMore, "translationX", -10f, -1000f, 0, 2000);
            function(buttonStop, "alpha", 1f, 0f, 500, 1000);
            function(buttonStop, "translationX", 10f, 1000f, 0, 2000);

            buttonPlayAgain.setEnabled(true);
            function(buttonPlayAgain, "alpha", 0f, 1f, 2000, 1000);
            function(textBankWinner, "alpha", 0, 1, 1000, 1000);
            function(textBankWinner, "scaleX", 1, 1, 1000, 1000);
            function(textBankWinner, "scaleY", 1, 1, 1000, 1000);
            bankWinner();


        }
    }


    public void comprobacionGanadorYou() {
        if (youScoreSum > 7.5) {
            function(buttonOneMore, "alpha", 1f, 0f, 500, 1000);
            function(buttonOneMore, "translationX", -10f, -1000f, 0, 2000);
            function(buttonStop, "alpha", 1f, 0f, 500, 1000);
            function(buttonStop, "translationX", 10f, 1000f, 0, 2000);

            buttonPlayAgain.setEnabled(true);
            function(buttonPlayAgain, "alpha", 0f, 1f, 2000, 1000);
            function(textBankWinner, "alpha", 0, 1, 1000, 1000);
            function(textBankWinner, "scaleX", 1, 1, 1000, 1000);
            function(textBankWinner, "scaleY", 1, 1, 1000, 1000);
            bankWinner();
        }

        if (youScoreSum == 7.5) {
            function(buttonOneMore, "alpha", 1f, 0f, 500, 1000);
            function(buttonOneMore, "translationX", -10f, -1000f, 0, 2000);
            function(buttonStop, "alpha", 1f, 0f, 500, 1000);
            function(buttonStop, "translationX", 10f, 1000f, 0, 2000);

            buttonPlayAgain.setEnabled(true);
            function(buttonPlayAgain, "alpha", 0f, 1f, 2000, 1000);
            function(textYouHaveWon, "alpha", 0, 1, 1000, 1000);
            function(textYouHaveWon, "scaleX", 1, 1, 1000, 1000);
            function(textYouHaveWon, "scaleY", 1, 1, 1000, 1000);
            youWinner();
        }
    }

    public int CardsGameFuction(int cardNumero) {
        ArrayList<Integer> arrayCards = new ArrayList<>();

        arrayCards.add(R.drawable.clubs01);
        arrayCards.add(R.drawable.clubs02);
        arrayCards.add(R.drawable.clubs03);
        arrayCards.add(R.drawable.clubs04);
        arrayCards.add(R.drawable.clubs05);
        arrayCards.add(R.drawable.clubs06);
        arrayCards.add(R.drawable.clubs07);
        arrayCards.add(R.drawable.clubs08);
        arrayCards.add(R.drawable.clubs09);
        arrayCards.add(R.drawable.clubs10);
        arrayCards.add(R.drawable.clubs11);
        arrayCards.add(R.drawable.clubs12);

        arrayCards.add(R.drawable.swords01);
        arrayCards.add(R.drawable.swords02);
        arrayCards.add(R.drawable.swords03);
        arrayCards.add(R.drawable.swords04);
        arrayCards.add(R.drawable.swords05);
        arrayCards.add(R.drawable.swords06);
        arrayCards.add(R.drawable.swords07);
        arrayCards.add(R.drawable.swords08);
        arrayCards.add(R.drawable.swords09);
        arrayCards.add(R.drawable.swords10);
        arrayCards.add(R.drawable.swords11);
        arrayCards.add(R.drawable.swords12);

        arrayCards.add(R.drawable.cups01);
        arrayCards.add(R.drawable.cups02);
        arrayCards.add(R.drawable.cups03);
        arrayCards.add(R.drawable.cups04);
        arrayCards.add(R.drawable.cups05);
        arrayCards.add(R.drawable.cups06);
        arrayCards.add(R.drawable.cups07);
        arrayCards.add(R.drawable.cups08);
        arrayCards.add(R.drawable.cups09);
        arrayCards.add(R.drawable.cups10);
        arrayCards.add(R.drawable.cups11);
        arrayCards.add(R.drawable.cups12);

        arrayCards.add(R.drawable.golds01);
        arrayCards.add(R.drawable.golds02);
        arrayCards.add(R.drawable.golds03);
        arrayCards.add(R.drawable.golds04);
        arrayCards.add(R.drawable.golds05);
        arrayCards.add(R.drawable.golds06);
        arrayCards.add(R.drawable.golds07);
        arrayCards.add(R.drawable.golds08);
        arrayCards.add(R.drawable.golds09);
        arrayCards.add(R.drawable.golds10);
        arrayCards.add(R.drawable.golds11);
        arrayCards.add(R.drawable.golds12);

        return arrayCards.get(cardNumero);
    }


    public float mapCardsValue(int key) {
        Map<Integer, Float> mapCards = new HashMap<Integer, Float>();

        mapCards.put(R.drawable.clubs01, 1.0f);
        mapCards.put(R.drawable.clubs02, 2.0f);
        mapCards.put(R.drawable.clubs03, 3.0f);
        mapCards.put(R.drawable.clubs04, 4.0f);
        mapCards.put(R.drawable.clubs05, 5.0f);
        mapCards.put(R.drawable.clubs06, 6.0f);
        mapCards.put(R.drawable.clubs07, 7.0f);
        mapCards.put(R.drawable.clubs08, 0.5f);
        mapCards.put(R.drawable.clubs09, 0.5f);
        mapCards.put(R.drawable.clubs10, 0.5f);
        mapCards.put(R.drawable.clubs11, 0.5f);
        mapCards.put(R.drawable.clubs12, 0.5f);

        mapCards.put(R.drawable.swords01, 1.0f);
        mapCards.put(R.drawable.swords02, 2.0f);
        mapCards.put(R.drawable.swords03, 3.0f);
        mapCards.put(R.drawable.swords04, 4.0f);
        mapCards.put(R.drawable.swords05, 5.0f);
        mapCards.put(R.drawable.swords06, 6.0f);
        mapCards.put(R.drawable.swords07, 7.0f);
        mapCards.put(R.drawable.swords08, 0.5f);
        mapCards.put(R.drawable.swords09, 0.5f);
        mapCards.put(R.drawable.swords10, 0.5f);
        mapCards.put(R.drawable.swords11, 0.5f);
        mapCards.put(R.drawable.swords12, 0.5f);

        mapCards.put(R.drawable.cups01, 1.0f);
        mapCards.put(R.drawable.cups02, 2.0f);
        mapCards.put(R.drawable.cups03, 3.0f);
        mapCards.put(R.drawable.cups04, 4.0f);
        mapCards.put(R.drawable.cups05, 5.0f);
        mapCards.put(R.drawable.cups06, 6.0f);
        mapCards.put(R.drawable.cups07, 7.0f);
        mapCards.put(R.drawable.cups08, 0.5f);
        mapCards.put(R.drawable.cups09, 0.5f);
        mapCards.put(R.drawable.cups10, 0.5f);
        mapCards.put(R.drawable.cups11, 0.5f);
        mapCards.put(R.drawable.cups12, 0.5f);
        //Golds
        mapCards.put(R.drawable.golds01, 1.0f);
        mapCards.put(R.drawable.golds02, 2.0f);
        mapCards.put(R.drawable.golds03, 3.0f);
        mapCards.put(R.drawable.golds04, 4.0f);
        mapCards.put(R.drawable.golds05, 5.0f);
        mapCards.put(R.drawable.golds06, 6.0f);
        mapCards.put(R.drawable.golds07, 7.0f);
        mapCards.put(R.drawable.golds08, 0.5f);
        mapCards.put(R.drawable.golds09, 0.5f);
        mapCards.put(R.drawable.golds10, 0.5f);
        mapCards.put(R.drawable.golds11, 0.5f);
        mapCards.put(R.drawable.golds12, 0.5f);
        return mapCards.get(key);
    }


    public void oneMoreFuction() {
        numeroCard = arrayCardsRandom.get(contadorCardsDades);
        actualImg = CardsGameFuction(numeroCard);
        switch (contadorCardsDades) {
            case 1:
                imageCard1You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard1You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard1You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard1You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard1You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 2:
                imageCard2You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard2You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard2You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard2You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard2You, "alpha", 0f, 1f, 2000, 500);
                restartImgMove();
                break;
            case 3:
                imageCard3You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard3You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard3You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard3You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard3You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 4:
                imageCard4You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard4You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard4You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard4You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard4You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 5:
                imageCard5You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard5You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard5You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard5You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard5You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 6:
                imageCard6You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard6You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard6You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard6You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard6You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 7:
                imageCard7You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard7You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard7You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard7You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard7You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 8:
                imageCard8You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard8You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard8You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard8You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard8You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 9:
                imageCard9You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard9You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard9You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard9You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard9You, "alpha", 0f, 1f, 2000, 500);
                break;

            case 10:
                imageCard10You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard10You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard10You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard10You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard10You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 11:
                imageCard11You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard11You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard11You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard11You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard11You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 12:
                imageCard12You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard12You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard12You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard12You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard12You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 13:
                imageCard13You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard13You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard13You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard13You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard13You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 14:
                imageCard14You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard14You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard14You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard14You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard14You, "alpha", 0f, 1f, 2000, 500);
                break;
            case 15:
                imageCard15You.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard15You.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard15You.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard15You, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard15You, "alpha", 0f, 1f, 2000, 500);
                break;
        }


        youScoreSum = youScoreSum + Double.valueOf(mapCardsValue(actualImg));
        textViewYouScore.postDelayed(new Runnable() {
            @Override
            public void run() {
                textViewYouScore.setText(String.valueOf(youScoreSum));
            }
        }, 2000);
        contadorCardsDades++;
        if (youScoreSum < 7.5) {
            function(buttonOneMore, "alpha", 0f, 1f, 1500, 1000);
            function(buttonOneMore, "translationX", -1000f, -10f, 1500, 2000);
            function(buttonStop, "alpha", 0f, 1f, 1500, 1000);
            function(buttonStop, "translationX", 1000f, 10f, 1500, 2000);
        }
    }

    public void restartImgMove() {
        function(imageCardMove, "x", imageCartasMazo.getX(), 0, 0);
        function(imageCardMove, "y", imageCartasMazo.getY(), 0, 0);
        function(imageCardMove, "alpha", 0f, 1f, 0, 0);
    }

    public void stopBankFunction() {
        numeroCard = arrayCardsRandom.get(contadorCardsDades);
        actualImg = CardsGameFuction(numeroCard);
        switch (contadorBank) {
            case 1:
                imageCard1Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard1Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard1Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard1Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard1Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 2:
                imageCard2Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard2Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard2Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard2Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard2Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 3:
                imageCard3Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard3Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard3Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard3Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard3Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 4:
                imageCard4Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard4Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard4Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard4Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard4Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 5:
                imageCard5Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard5Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard5Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard5Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard5Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 6:
                imageCard6Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard6Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard6Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard6Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard6Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 7:
                imageCard7Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard7Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard7Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard7Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard7Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 8:
                imageCard8Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard8Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard8Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard8Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard8Bank, "alpha", 0f, 1f, 2000, 500);
            case 9:
                imageCard9Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard9Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard9Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard9Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard9Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 10:
                imageCard10Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard10Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard10Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard10Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard10Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 11:
                imageCard11Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard11Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard11Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard11Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard11Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 12:
                imageCard12Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard12Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard12Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard12Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard12Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 13:
                imageCard13Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard13Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard13Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard13Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard13Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 14:
                imageCard14Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard14Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard14Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard14Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard14Bank, "alpha", 0f, 1f, 2000, 500);
                break;
            case 15:
                imageCard15Bank.setImageResource(actualImg);
                function(imageCardMove, "x", imageCard15Bank.getX(), 500, 1000);
                function(imageCardMove, "y", imageCard15Bank.getY(), 500, 1000);
                function(imageCardMove, "rotationY", 0f, -180f, 1500, 1000);
                function(imageCard15Bank, "rotationY", 180f, 0f, 1500, 1000);
                function(imageCardMove, "alpha", 1f, 0f, 2000, 500);
                function(imageCard15Bank, "alpha", 0f, 1f, 2000, 500);
                break;
        }


        bankScoreSum = bankScoreSum + Double.valueOf(mapCardsValue(actualImg));
        //textViewBankScore.setText(String.valueOf(bankScoreSum));
        textViewBankScore.postDelayed(new Runnable() {
            @Override
            public void run() {
                textViewBankScore.setText(String.valueOf(bankScoreSum));
            }
        }, 2000);
        contadorBank++;
        contadorCardsDades++;

    }

    public void read() {
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, MODE_PRIVATE);
        contadorGlobalBank = sharedPreferences.getInt("Bank", 0);
        contadorGlobalYou = sharedPreferences.getInt("You", 0);
        bankTotalScoreTextView.setText(String.valueOf(contadorGlobalBank));
        youTotalScoreTextView.setText(String.valueOf(contadorGlobalYou));
    }

    public void createSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("Bank", 0);
        editor.putInt("You", 0);
        editor.commit();
    }


    public void bankWinner() {
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        contadorGlobalBank = contadorGlobalBank + 1;
        editor.putInt("Bank", contadorGlobalBank);
        editor.putInt("You", contadorGlobalYou);
        editor.commit();
    }

    public void youWinner() {
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        contadorGlobalYou = contadorGlobalYou + 1;
        editor.putInt("Bank", contadorGlobalBank);
        editor.putInt("You", contadorGlobalYou);
        editor.commit();

    }

}