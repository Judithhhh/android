package com.example.radiogroup;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private RadioGroup radioGroup3;
    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private RadioButton radioButton3;
    private RadioButton radioButton4;
    private RadioButton radioButton5;
    private RadioButton radioButton6;
    private RadioButton radioButton7;
    private RadioButton radioButton8;
    private RadioButton radioButton9;
    private Button buttonCheck;
    private Switch Switch1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        radioGroup3 = (RadioGroup) findViewById(R.id.radioGroup3);
        radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
        radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
        radioButton3 = (RadioButton) findViewById(R.id.radioButton3);
        radioButton4 = (RadioButton) findViewById(R.id.radioButton4);
        radioButton5 = (RadioButton) findViewById(R.id.radioButton5);
        radioButton6 = (RadioButton) findViewById(R.id.radioButton6);
        radioButton7 = (RadioButton) findViewById(R.id.radioButton7);
        radioButton8 = (RadioButton) findViewById(R.id.radioButton8);
        radioButton9 = (RadioButton) findViewById(R.id.radioButton9);
        buttonCheck = (Button) findViewById(R.id.buttonCheck);
        Switch1 = (Switch) findViewById(R.id.switch1);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selected = radioGroup1.getCheckedRadioButtonId();
                switch (selected){
                    case R.id.radioButton1:
                        Toast.makeText(MainActivity.this, "Opcion 1", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton2:
                        Toast.makeText(MainActivity.this, "Opcion 2", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton3:
                        Toast.makeText(MainActivity.this, "Opcion 3", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selected = radioGroup2.getCheckedRadioButtonId();
                switch (selected){
                    case R.id.radioButton4:
                        Toast.makeText(MainActivity.this, "Opcion 4", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton5:
                        Toast.makeText(MainActivity.this, "Opcion 5", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton6:
                        Toast.makeText(MainActivity.this, "Opcion 6", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selected = radioGroup3.getCheckedRadioButtonId();
                switch (selected){
                    case R.id.radioButton7:
                        Toast.makeText(MainActivity.this, "Opcion 7", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton8:
                        Toast.makeText(MainActivity.this, "Opcion 8", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton9:
                        Toast.makeText(MainActivity.this, "Opcion 9", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });


        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selected1 = radioGroup1.getCheckedRadioButtonId();
                //hook
                RadioButton radioButton1 = findViewById(selected1);
//                String selection1 = radioButton1.getText().toString();

                int selected2 = radioGroup2.getCheckedRadioButtonId();
                //hook
                RadioButton radioButton2 = findViewById(selected2);
//                String selection2 = radioButton1.getText().toString();

                int selected3 = radioGroup3.getCheckedRadioButtonId();
                //hook
                RadioButton radioButton3 = findViewById(selected3);
//                String selection3 = radioButton1.getText().toString();


                Toast.makeText(MainActivity.this, radioButton1.getText() + ",  " +
                        radioButton2.getText() + ",  " + radioButton3.getText(), Toast.LENGTH_SHORT).show();
            }
        });



        Switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for (int i = 0; i < radioGroup3.getChildCount(); i++) {
                    ((RadioButton) radioGroup3.getChildAt(i)).setEnabled(isChecked);
                }

            }
        });

        for (int i = 0; i < radioGroup3.getChildCount(); i++) {
            ((RadioButton) radioGroup3.getChildAt(i)).setEnabled(false);
        }
    }
}