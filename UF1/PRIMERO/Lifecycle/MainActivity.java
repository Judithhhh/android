package com.example.lifecycle;
//JUDITH ARCHILLA

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String tag = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, R.string.create, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, R.string.start, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity started");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, R.string.resume, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, R.string.pause, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, R.string.stop, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity stoped");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Toast.makeText(this, R.string.restart, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity restarted");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, R.string.destroy, Toast.LENGTH_LONG).show();

        Log.i(tag, "Activity is being destroyed");
    }

    public void secondActivity(View view) {
        Intent in = new Intent(MainActivity.this, SecondActivity.class);
        startActivity(in);
        finish();
    }
}