package com.example.a18b;

import androidx.appcompat.app.AppCompatActivity;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mplayer;

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;
    private ImageView imageView6;
    private ImageView imageView7;
    private ImageView imageView9;
    private ImageView imageView10;
    private ImageView imageView11;
    private LinearLayout layout;
    private TextView text1;

    private int image1 = 0;
    private int image2 = 0;
    private int image3 = 0;
    private int image4 = 0;
    private int image5 = 0;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;
    ObjectAnimator objectAnimator5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView6 = (ImageView) findViewById(R.id.imageView6);
        imageView7 = (ImageView) findViewById(R.id.imageView7);
        imageView9 = (ImageView) findViewById(R.id.imageView9);
        imageView10 = (ImageView) findViewById(R.id.imageView10);
        imageView11 = (ImageView) findViewById(R.id.imageView11);
        layout = (LinearLayout) findViewById(R.id.layout);
        text1 = (TextView) findViewById(R.id.text1);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image1 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView1, "alpha", 1f, 0f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView6, "RotationY", 0f, 180f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView6, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
//                  set.setTarget(v);
//                  set.start();
                    image1++;
                } else {
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView1, "alpha", 0f, 1f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView6, "RotationY", 180f, 0f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView6, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
//                  set.setTarget(v);
//                  set.start();
                    image1 = 0;
                }
//              objectAnimator1.setDuration(1000);
//              objectAnimator1.start();
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                animatorSet.start();
                mplayer = MediaPlayer.create(MainActivity.this, R.raw.song1);
                mplayer.start();
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image2 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView2, "RotationX", 0f, 180f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView2, "alpha", 1f, 0f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView7, "RotationX", 0f, 180f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView7, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
//                  set.setTarget(v);
//                  set.start();
                    image2++;
                } else {
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView2, "RotationX", 180f, 0f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView2, "alpha", 0f, 1f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView7, "RotationX", 180f, 0f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView7, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
//                  set.setTarget(v);
//                  set.start();
                    image2 = 0;
                }
//              objectAnimator1.setDuration(1000);
//              objectAnimator1.start();
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                animatorSet.start();
                mplayer = MediaPlayer.create(MainActivity.this, R.raw.song2);
                mplayer.start();
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image3 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView3, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView3, "alpha", 1f, 0f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView9, "RotationY", 0f, 180f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView9, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
//                  set.setTarget(v);
//                  set.start();
                    image3++;
                } else {
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView3, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView3, "alpha", 0f, 1f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView9, "RotationY", 180f, 0f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView9, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
//                  set.setTarget(v);
//                  set.start();
                    image3 = 0;
                }
//              objectAnimator1.setDuration(1000);
//              objectAnimator1.start();
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                animatorSet.start();
                mplayer = MediaPlayer.create(MainActivity.this, R.raw.song3);
                mplayer.start();
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image4 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView4, "RotationX", 0f, 180f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView4, "alpha", 1f, 0f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView10, "RotationX", 0f, 180f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView10, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
//                  set.setTarget(v);
//                  set.start();
                    image4++;
                } else {
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView4, "RotationX", 180f, 0f);
                    objectAnimator1.setDuration(1000); // un segundo
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView4, "alpha", 0f, 1f);
                    objectAnimator2.setStartDelay(500); // medio segundo

                    objectAnimator3 = ObjectAnimator.ofFloat(imageView10, "RotationX", 180f, 0f);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView10, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1); // milisegundo
//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
//                  set.setTarget(v);
//                  set.start();
                    image4 = 0;
                }
//              objectAnimator1.setDuration(1000);
//              objectAnimator1.start();
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                animatorSet.start();
                mplayer = MediaPlayer.create(MainActivity.this, R.raw.song4);
                mplayer.start();
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image5 == 0){
                        objectAnimator1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 0f, 180f);
                        objectAnimator1.setDuration(1000); // un segundo
                        objectAnimator2 = ObjectAnimator.ofFloat(imageView5, "alpha", 1f, 0.3f);
                        objectAnimator2.setStartDelay(500); // medio segundo

                        objectAnimator3 = ObjectAnimator.ofFloat(layout, "RotationY", -180f, 0f);
                        objectAnimator3.setDuration(1000);
                        objectAnimator4 = ObjectAnimator.ofFloat(layout, "alpha", 0f, 1f);
                        objectAnimator4.setStartDelay(500);
                        objectAnimator4.setDuration(1); // milisegundo

//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
//                  set.setTarget(v);
//                  set.start();
                        image5++;
                    } else {
                        objectAnimator1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 180f, 0f);
                        objectAnimator1.setDuration(1000); // un segundo
                        objectAnimator2 = ObjectAnimator.ofFloat(imageView5, "alpha", 0.3f, 1f);
                        objectAnimator2.setStartDelay(500); // medio segundo

                        objectAnimator3 = ObjectAnimator.ofFloat(layout, "RotationY", 0, -180f);
                        objectAnimator3.setDuration(1000);
                        objectAnimator4 = ObjectAnimator.ofFloat(layout, "alpha", 1f, 0f);
                        objectAnimator4.setStartDelay(500);
                        objectAnimator4.setDuration(1); // milisegundo

//                  Animator set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
//                  set.setTarget(v);
//                  set.start();
                        image5 = 0;
                          }
//                  objectAnimator1.setDuration(1000);
//                  objectAnimator1.start();
                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                    animatorSet.start();
                    mplayer = MediaPlayer.create(MainActivity.this, R.raw.song5);
                    mplayer.start();
                }
            });



    }
}