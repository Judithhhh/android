package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {
    private ImageView logo1;
    private ImageView image1;
    private TextView textView;
    public static final int SPLASH_SCREEN = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        image1 = findViewById(R.id.image1);

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                startActivity(intent);
            }
        });


        //Hooks
        logo1 = findViewById(R.id.logo1);
        textView = findViewById(R.id.textView);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(logo1, "app_name");
                pairs[1] = new Pair<View, String>(textView, "textTitle");

                Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                            DashboardActivity.this, pairs
                    );

                    startActivity(intent, options.toBundle());
                }

            }
        };
    }
}