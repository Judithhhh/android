package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservaActivity extends AppCompatActivity {

    private Spinner spinnerRefugi;

    private RadioGroup radioGroup3;
    private RadioButton btvege;
    private RadioButton btsingluten;
    private RadioButton btceliac;
    private RadioButton btcap;
    private Switch aSwitch;
    private EditText entradaDate;
    private EditText sortidaDate;
    DatePickerDialog picker;

    private ImageView imageView11;
    private CardView cardView1;
    private CardView cardView2;
    private CardView cardView3;
    private CardView cardView4;
    private CardView cardView5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        radioGroup3 = findViewById(R.id.radioGroup3);
        btvege = (RadioButton) findViewById(R.id.btvege);
        btsingluten = (RadioButton) findViewById(R.id.btsingluten);
        btceliac = (RadioButton) findViewById(R.id.btceliac);
        btcap = (RadioButton) findViewById(R.id.btcap);
        aSwitch = findViewById(R.id.aSwitch);
        imageView11 = (ImageView) findViewById(R.id.imageView11);

        cardView1 = (CardView) findViewById(R.id.cardView1);
        cardView2 = (CardView) findViewById(R.id.cardView2);
        cardView3 = (CardView) findViewById(R.id.cardView3);
        cardView4 = (CardView) findViewById(R.id.cardView4);
        cardView5 = (CardView) findViewById(R.id.cardView5);


        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imageView11 = R.drawable.foto1;
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imageView11 = R.drawable.foto2;
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imageView11 = R.drawable.foto3;
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imageView11 = R.drawable.foto4;
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imageView11 = R.drawable.foto5;
            }
        });


        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selected = radioGroup3.getCheckedRadioButtonId();
                switch (selected) {
                    case R.id.btvege:
                        Toast.makeText(ReservaActivity.this, "Vegetarià", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.btsingluten:
                        Toast.makeText(ReservaActivity.this, "Sense Gluten ", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.btceliac:
                        Toast.makeText(ReservaActivity.this, "Celíac", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.btcap:
                        Toast.makeText(ReservaActivity.this, "Cap", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        break;
                }
            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(ReservaActivity.this, "enable",Toast.LENGTH_SHORT).show();
                for (int i = 0; i < radioGroup3.getChildCount(); i++) {
                    ((RadioButton) radioGroup3.getChildAt(i)).setEnabled(isChecked);
                }

            }
        });

        Toast.makeText(ReservaActivity.this, "disable",Toast.LENGTH_SHORT).show();
        for (int i = 0; i < radioGroup3.getChildCount(); i++) {
            ((RadioButton) radioGroup3.getChildAt(i)).setEnabled(false);
        }




        entradaDate = (EditText) findViewById(R.id.entradaDate);
        sortidaDate = (EditText) findViewById(R.id.sortidaDate);

        entradaDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Select actual date
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                //DataPickerDialog
                //DataPickerDialog.OnDateSetListener()

                picker = new DatePickerDialog(ReservaActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            //mostrar la data seleccionada
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                entradaDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        sortidaDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Select actual date
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                //DataPickerDialog
                //DataPickerDialog.OnDateSetListener()

                picker = new DatePickerDialog(ReservaActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            //mostrar la data seleccionada
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                entradaDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        spinnerRefugi = (Spinner) findViewById(R.id.spinnerRefugi);

        List<String> refugi = new ArrayList<>();
        refugi.add(0, "Selecciona un Refugi:");
        refugi.add("Refugi Josep Maria Blanc");
        refugi.add("Refugi Cap de Llauset");
        refugi.add("Refugi Ventosa i Clavell");
        refugi.add("Refugi Amitges");
        refugi.add("Refugi Josep Maria Montfort");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                refugi);

        spinnerRefugi.setAdapter(dataAdapter);

        spinnerRefugi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String refugiSelected = (String) parent.getItemAtPosition(position);
                Toast.makeText(ReservaActivity.this, refugiSelected, Toast.LENGTH_SHORT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(ReservaActivity.this, "Nothing selected", Toast.LENGTH_SHORT);
            }
        });

    }
}