package com.example.calculadora;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //private Button btClickme;

    private static final String HOME_ACTIVITY_TAG = MainActivity.class.getSimpleName();

   @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        //btClickme = (Button) findViewById(R.id.btClickme);

        Toast toast = Toast.makeText(getApplicationContext(),
                "onCreate method called",
                Toast.LENGTH_LONG);
        toast.show();

        showLog("Activity Created");
    }

    @Override
    protected void onStart(){
        Toast toast = Toast.makeText(getApplicationContext(),
                "onStart method called",
                Toast.LENGTH_LONG);
        toast.show();
        super.onStart();
        showLog ("Activity Started");
    }

    @Override
    protected void onRestart(){
        Toast toast = Toast.makeText(getApplicationContext(),
                "onRestart method called",
                Toast.LENGTH_LONG);
        toast.show();
        super.onRestart();
        showLog ("Activity Restarted");
    }

    @Override
    protected void onResume(){
        Toast toast = Toast.makeText(getApplicationContext(),
                "onResume method called",
                Toast.LENGTH_LONG);
        toast.show();
        super.onResume();
        showLog ("Activity Resumed");
    }

    @Override
    protected void onPause(){
        Toast toast = Toast.makeText(getApplicationContext(),
                "onPause method called",
                Toast.LENGTH_LONG);
        toast.show();
        super.onPause();
        showLog ("Activity Paused");
    }

    @Override
    protected void onStop(){
        Toast toast = Toast.makeText(getApplicationContext(),
                "onStop method called",
                Toast.LENGTH_LONG);
        toast.show();
        super.onStop();
        showLog ("Activity Stopped");
    }

    @Override
    protected void onDestroy(){
        Toast toast = Toast.makeText(getApplicationContext(),
                "onDestroy method called",
                Toast.LENGTH_LONG);
        toast.show();
        super.onDestroy();
        showLog ("Activity is being destroyed");
    }

    private void showLog(String text){
        Log.e(HOME_ACTIVITY_TAG, text);
        Log.w(HOME_ACTIVITY_TAG, text);
        Log.i(HOME_ACTIVITY_TAG, text);
        Log.d(HOME_ACTIVITY_TAG, text);
        Log.v(HOME_ACTIVITY_TAG, text);
    }

    /*public void Clickme(View view){
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        startActivity(intent);
    }*/


    public static String EXTRA_TEXT = "com.example.calculadora.EXTRA_TEXT";
    public static String EXTRA_INT = "com.example.calculadora.EXTRA_INT";


    private TextView textResult;
    private EditText num1;
    private EditText num2;
    private Button btAdd;
    private Button btSubst;
    private Button btMult;
    private Button btDiv;
    private Button btPower;
    private Button btCe;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook = assinacions/enllaços

        textResult = (TextView) findViewById(R.id.textResult);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        btAdd = (Button) findViewById(R.id.btAdd);
        btSubst = (Button) findViewById(R.id.btSubst);
        btMult = (Button) findViewById(R.id.btMult);
        btDiv = (Button) findViewById(R.id.btDiv);
        btPower = (Button) findViewById(R.id.btPower);
        btCe = (Button) findViewById(R.id.btCe);

        textResult.setText("0.0");
        Log.e(MainActivity.class.getSimpleName(), "Mensaje personalizado");
        Log.w(MainActivity.class.getSimpleName(), "Mensaje personalizado");
        Log.i(MainActivity.class.getSimpleName(), "Mensaje personalizado");
        Log.d(MainActivity.class.getSimpleName(), "Mensaje personalizado");
        Log.v(MainActivity.class.getSimpleName(), "Mensaje personalizado");

        //ADD
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();

                } else {
                    double resultAdd = Double.parseDouble(num1.getText().toString().trim())
                            + Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultAdd + "");
                }
            }
        });

        //SUBST
        btSubst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();
                } else {
                    double resultSubst = Double.parseDouble(num1.getText().toString().trim())
                            - Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultSubst + "");
                }
            }
        });

        //MULT
        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(num1.getText().toString().trim().isEmpty() ||
                num2.getText().toString().trim().isEmpty()){
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();
                } else {
                    double resultMult = Double.parseDouble(num1.getText().toString().trim())
                        * Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultMult + "");
                }
            }
        });

        //DIV
        btDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                num2.getText().toString().trim().isEmpty()){
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();
                } else if (num2.getText().toString().trim().equals("0") ||
                    num2.getText().toString().trim().equals("0.0")) {
                    Toast.makeText(MainActivity.this, "Divide by zero", Toast.LENGTH_SHORT).show();
                } else {
                    double resultDiv = Double.parseDouble(num1.getText().toString().trim())
                            / Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultDiv + "");
                }
            }
        });

        //POWER
        btPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                num2.getText().toString().trim().isEmpty()) {
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();
                } else {
                    double resultPower = Math.pow(Double.parseDouble(num1.getText().toString().trim()),
                        Double.parseDouble(num2.getText().toString().trim()));
                    textResult.setText(resultPower + "");
                }
            }
        });

        //CE --> ARREGLAR
        btCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();
                } else {
                    String resultCe = "0.0";
                    textResult.setText(resultCe + "");
                }
            }
        });

    }

    public void moreInfo (View view) {
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra(EXTRA_TEXT, "This is a message for new Activity");
        intent.putExtra(EXTRA_INT, 17);
        startActivity(intent);
    }
}