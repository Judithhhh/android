package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private TextView textInfo;
    private TextView textInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textInfo = (TextView) findViewById(R.id.textInfo);
        textInt = (TextView) findViewById(R.id.textInt);

        Intent intent = getIntent();

        String text = intent.getStringExtra(MainActivity.EXTRA_TEXT);
        int number = intent.getIntExtra(MainActivity.EXTRA_INT, 0);
        // el zero es por si esta nulo o vacio, que salga por defecto

        textInfo.setText(text);
        textInt.setText(number + "");
    }
}