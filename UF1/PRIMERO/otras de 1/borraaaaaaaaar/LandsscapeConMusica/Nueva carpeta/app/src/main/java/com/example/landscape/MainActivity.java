package com.example.landscape;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Property;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mplayer;

    Boolean foo = true;

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;

    private int image1 = 0;
    private int image2 = 0;
    private int image3 = 0;
    private int image4 = 0;
    private int image5 = 0;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;
    ObjectAnimator objectAnimator5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    imageView1 = (ImageView) findViewById(R.id.imageView1);
    imageView2 = (ImageView) findViewById(R.id.imageView2);
    imageView3 = (ImageView) findViewById(R.id.imageView3);
    imageView4 = (ImageView) findViewById(R.id.imageView4);
    imageView5 = (ImageView) findViewById(R.id.imageView5);

    imageView1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           // objectAnimator1 = ObjectAnimator.ofFloat(image1, Property.of())

/*            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
            set.setTarget(v);
            set.start();*/

            mplayer = MediaPlayer.create(MainActivity.this, R.raw.song1);


           if(foo=true){
/*                objectAnimator1 = ObjectAnimator.ofFloat(image1, "rotation", 0f, 180f);
                image1++;*/
               AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
               set.setTarget(v);
               set.start();
               foo = false;
            } else if (foo=false){
/*                objectAnimator1 = ObjectAnimator.ofFloat(image1, "rotation", 180f, 0f);
                image1 = 0;*/
               AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
               set.setTarget(v);
               set.start();
               foo=true;
            }

           mplayer.start();

/*            objectAnimator1.setDuration(1000);
            objectAnimator1.start();*/
        }
    });


    }
}