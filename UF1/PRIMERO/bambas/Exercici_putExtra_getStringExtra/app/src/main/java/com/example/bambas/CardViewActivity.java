package com.example.bambas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CardViewActivity extends AppCompatActivity {

    private TextView textTitle;
    private TextView textSubtitle;
    private TextView rating;
    private TextView favorites;
    private TextView downloads;
    private TextView productName;
    private TextView productDesc;
    private TextView productPrice;
    private TextView productImage;
    private ImageView imageTop;
    private ImageView imageView11;
    private TextView productLongText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        //hook
        textTitle = (TextView) findViewById(R.id.textTitle);
        textSubtitle = (TextView) findViewById(R.id.textSubtitle);
        rating = (TextView) findViewById(R.id.rating);
        favorites = (TextView) findViewById(R.id.favorites);
        downloads = (TextView) findViewById(R.id.downloads);
        productDesc = (TextView) findViewById(R.id.productDesc);
        productName = (TextView) findViewById(R.id.productName);
        productPrice = (TextView) findViewById(R.id.price);
        imageView11 = (ImageView) findViewById(R.id.imageView11);
        imageTop = (ImageView) findViewById(R.id.imageTop);
        productLongText = (TextView) findViewById(R.id.productLongText);


        Intent i = getIntent();
        String sRating = i.getStringExtra(MainActivity.RATING);
        String sFavorites = i.getStringExtra(MainActivity.FAVORITES);
        String sDownloads = i.getStringExtra(MainActivity.DOWNLOADS);
        String sProductTitle = i.getStringExtra(MainActivity.PRODUCT_TITLE);
        String sProductName = i.getStringExtra(MainActivity.PRODUCT_NAME);
        String sProductDesc = i.getStringExtra(MainActivity.PRODUCT_DESC);
        String sProductPrice = i.getStringExtra(MainActivity.PRODUCT_PRICE);
        String sProductLongText = i.getStringExtra(MainActivity.PRODUCT_LONG_TEXT);

        int iImageView11 = i.getIntExtra(MainActivity.PRODUCT_IMAGE, 0);

        textTitle.setText(sProductTitle);
        rating.setText(sRating.toString());
        favorites.setText(sFavorites.toString());
        downloads.setText(sDownloads.toString());
        productName.setText(sProductName.toString());
        productDesc.setText(sProductDesc.toString());
        productPrice.setText(sProductPrice.toString());
        productLongText.setText(sProductLongText.toString());

        imageView11.setImageResource(iImageView11);
        imageTop.setImageResource(iImageView11);

    }
}