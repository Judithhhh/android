package com.example.bambas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public static String RATING = "com.example.bambas.RATING";
    public static String FAVORITES = "com.example.bambas.FAVORITES";
    public static String DOWNLOADS = "com.example.bambas.DOWNLOADS";
    public static String PRODUCT_IMAGE = "com.example.bambas.PRODUCT_IMAGE";
    public static String PRODUCT_TITLE = "com.example.bambas.PRODCUT_TITLE";
    public static String PRODUCT_NAME = "com.example.bambas.PRODUCT_NAME";
    public static String PRODUCT_DESC = "com.example.bambas.PRODUCT_DESC";
    public static String PRODUCT_PRICE = "com.example.bambas.PRODUCT_PRICE";
    public static String PRODUCT_LONG_TEXT = "com.example.bambas.PRODUCT_LONG_TEXT";

    private ImageView imageView11;
    private CardView cardView11;
    private CardView cardView12;
    private CardView cardView13;

    private CardView cardView21;
    private CardView cardView22;
    private CardView cardView23;

    private CardView cardView31;
    private CardView cardView32;
    private CardView cardView33;

    private CardView cardView41;
    private CardView cardView42;
    private CardView cardView43;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imageView11 = (ImageView) findViewById(R.id.imageView11);
        cardView11 = (CardView) findViewById(R.id.cardView11);
        cardView12 = (CardView) findViewById(R.id.cardView12);
        cardView13 = (CardView) findViewById(R.id.cardView13);

        cardView21 = (CardView) findViewById(R.id.cardView21);
        cardView22 = (CardView) findViewById(R.id.cardView22);
        cardView23 = (CardView) findViewById(R.id.cardView23);

        cardView31 = (CardView) findViewById(R.id.cardView31);
        cardView32 = (CardView) findViewById(R.id.cardView32);
        cardView33 = (CardView) findViewById(R.id.cardView33);

        cardView41 = (CardView) findViewById(R.id.cardView41);
        cardView42 = (CardView) findViewById(R.id.cardView42);
        cardView43 = (CardView) findViewById(R.id.cardView43);

        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 8/10";
                String favorites = "Favorites: 200";
                String downloads = "Downloads: 156";
                String productTitle = "Vans";
                String productName = "SK8-HI 2.0 CON PLATAFORMA";
                String productDesc = "Style handcraft";
                String productPrice = "94,99$";
                String productLongText = "DESCRIPCIÓN Y CARACTERÍSTICAS:\n" +
                        "Las zapatillas Sk8-Hi 2.0 con plataforma tienen el legendario diseño de la zapatilla alta de cordones, " +
                        "con una pala de cuero y ante resistente, puntera reforzada para soportar el desgaste, cuello acolchado " +
                        "para ofrecer sujeción y flexibilidad y la distintiva suela waffle de goma con plataforma.\n" +
                        "\n" +
                        "Composition: Lona y cuero";
                int imageView11 = R.drawable.vanscuadros;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });


        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 7/10";
                String favorites = "Favorites: 280";
                String downloads = "Downloads: 145";
                String productTitle = "Vans";
                String productName = "OLD SKOOL DE PLATAFORMA";
                String productDesc = "Style handcraft";
                String productPrice = "74,99$";
                String productLongText = "DESCRIPCIÓN Y CARACTERÍSTICAS:\n" +
                        "Las Old Skool de plataforma combinan el diseño clásico de las zapatillas de skate con banda lateral " +
                        "con una pala de ante y lona resistente, puntera reforzada para resistir el desgaste, cuello acolchado " +
                        "para mayor sujeción y flexibilidad, y la distintiva suela waffle de caucho de plataforma.\n" +
                        "\n" +
                        "Material:52 % cuero, 48 % lona";
                int imageView11 = R.drawable.oldskool;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });

        cardView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 5/10";
                String favorites = "Favorites: 112";
                String downloads = "Downloads: 65";
                String productTitle = "Nike";
                String productName = "Nike Blazer Mid '77 Vintage";
                String productDesc = "Style handcraft";
                String productPrice = "99.99$";
                String productLongText = "ESTILO CLÁSICO INSPIRADO EN EL BALONCESTO.\n" +
                        "Luce las Nike Blazer Mid Vintage '77, un clásico de la cancha renovado " +
                        "con colores elegantes. La piel proporciona un look premium y añade durabilidad a este icono del baloncesto.\n" +
                        "\n" +
                        "Ventajas:\n" +
                        "Piel en la parte superior para ofrecer durabilidad.\n" +
                        "Confección en autoclave que une la suela exterior con la mediasuela para un look estilizado";

                int imageView11 = R.drawable.blazer;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });

//-----------------------------------------------------------------------------------------------

        cardView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 6/10";
                String favorites = "Favorites: 40";
                String downloads = "Downloads: 16";
                String productTitle = "Nike";
                String productName = "Nike Cortez";
                String productDesc = "Style handcraft";
                String productPrice = "84,99$";
                String productLongText = "SIENTE LA HISTORIA DE LAS ZAPATILLAS." +
                        "\n" +
                        "Las Nike Cortez Basic están inspiradas en el modelo original de 1972. Aunque han pasado por muchas versiones, " +
                        "esta última cuenta con piel en la parte superior para volver a las raíces del estilo de este icono.\n" +
                        "\n" +
                        "Ventajas:\n" +
                        "Piel sintética y auténtica en la parte superior para una mayor durabilidad y un tacto premium.\n" +
                        "Zona del tobillo de corte bajo para un ajuste natural.\n" +
                        "Suela exterior de goma con diseño de espiga para proporcionar una mayor tracción y durabilidad.";
                int imageView11 = R.drawable.cortez;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });


        cardView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 9/10";
                String favorites = "Favorites: 280";
                String downloads = "Downloads: 145";
                String productTitle = "Nike";
                String productName = "Nike Air Max 270";
                String productDesc = "Style handcraft";
                String productPrice = " 149,99$";
                String productLongText = "DISEÑO AIR LEGENDARIO CON MÁS ELEVACIÓN.\n" +
                        "Las primeras Air Max para el día a día te ofrecen más estilo, comodidad y una gran actitud en las Nike Air Max 270. " +
                        "El diseño se inspira en los iconos de Air Max para destacar la innovación más importante de Nike con una amplia ventana y una gran " +
                        "variedad de colores modernos.\n" +
                        "\n" +
                        "Ventajas:\n" +
                        "Unidad Max Air 270 para proporcionar una comodidad sin igual durante todo el día.\n" +
                        "Tejido Woven y sintético en la parte superior para ofrecer una mayor ligereza y ventilación.\n" +
                        "Mediasuela de espuma suave y cómoda.\n" +
                        "Funda interior elástica y confección de botín para crear un ajuste personalizable.\n" +
                        "Suela exterior de goma para una mayor tracción y durabilidad.\n" +
                        "\n" +
                        "Detalles del producto:\n" +
                        "Presilla\n" +
                        "Suela de goma\n" +
                        "Zona del tobillo acolchada";
                int imageView11 = R.drawable.airmax_negras;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });

        cardView23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 6/10";
                String favorites = "Favorites: 152";
                String downloads = "Downloads: 68";
                String productTitle = "Jordan";
                String productName = "Jordan 1 Retro High OG";
                String productDesc = "Style handcraft";
                String productPrice = "129.99$";
                String productLongText = "Esta nueva iteración del AJ1 Retro High OG combina " +
                        "el diseño tradicional de Jordan con dos de los colorways más codiciados en el mundo de las sneakers: el 'Baroque Brown' " +
                        "y el 'Racer Pink'." +
                        "\n" +
                        "Su atractivo look, inspirado en el vertiginoso ritmo de vida de Tokio, está logrado con un upper en capas de gamuza de lujo, " +
                        "una paleta de colores innovadores y elementos insignias clásicos, como la etiqueta tejida de la lengüeta y el Swoosh metalizado del costado." +
                        "\n" +
                        "Además, este par está acondicionada con una unidad Air bajo el talón que proporciona la misma amortiguación ligera que disfrutaba Michael " +
                        "Jordan mientras jugaba.";
                int imageView11 = R.drawable.jordan_azules;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });


//-----------------------------------------------------------------------------------------------

        cardView31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 5/10";
                String favorites = "Favorites: 40";
                String downloads = "Downloads: 16";
                String productTitle = "Nike";
                String productName = "Nike Air Max 2090";
                String productDesc = "Style handcraft";
                String productPrice = "149.99$";
                String productLongText = "UN SALTO FUTURISTA HACIA DELANTE.\n" +
                        "Lleva el pasado al futuro con las Nike Air Max 2090, un look atrevido inspirado en el ADN de las icónicas Air Max 90. " +
                        "La amortiguación Nike Air nueva en la planta del pie añade una comodidad sin igual y la malla transparente y la parte superior " +
                        "de tela de colores vivos se combina con las características originales atemporales para ofrecer un look vanguardista y moderno.\n" +
                        "\n" +
                        "Look innovador\n" +
                        "Actualización de las icónicas Air Max 90, que se atreven a ser mejores que ayer. La suela de goma con diseño de imitación " +
                        "tipo gofre cuenta con ranuras flexibles de gran tamaño para una mayor tracción y durabilidad.\n" +
                        "\n" +
                        "Comodidad para el día a día\n" +
                        "El diseño interno del botín y el talón acolchado ofrecen un ajuste ceñido y cómodo, y la mediasuela de espuma Cushlon " +
                        "ofrece suavidad durante todo el día.\n" +
                        "\n" +
                        "Diseño Air más grande\n" +
                        "Más Air en la planta del pie ofrece una pisada más suave y cómoda. El material TRU translúcido y elástico enmarca el " +
                        "diseño Air y ofrece un look futurista y llamativo.\n" +
                        "\n" +
                        "Ajuste rápido\n" +
                        "Nuevo diseño de sistema de cordones rápido para atarlas con rapidez y emprender tu camino.\n" +
                        "\n" +
                        "Detalles del producto\n" +
                        "Presilla en el talón\n" +
                        "Logotipo \"AIR\" en relieve en el talón para ofrecer un look duradero";
                int imageView11 = R.drawable.airmax_color;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });


        cardView32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 9/10";
                String favorites = "Favorites: 280";
                String downloads = "Downloads: 145";
                String productTitle = "Adidas";
                String productName = "Yeezy Boost 350 V2 \"Yecheil";
                String productDesc = "Style handcraft";
                String productPrice = "660$";
                String productLongText = "Zapatillas Yeezy Boost 350 V2 Yecheil de color negro de adidas YEEZY con puntera redonda, suela de goma, cierre con cordón y plantilla con logo. " +
                        "Estos estilos son suministrados por un marketplace de zapatillas premium que comercializa productos singulares, ya agotados y sin usar. " +
                        "Cada producto es rigurosamente inspeccionado por expertos que garantizan su autenticidad. \n " +
                        "\n" +
                        "Adidas Group recibe una puntuación de 4 sobre 5 por parte de la agencia ética Good On You. " +
                        "La marca utiliza materiales ecológicos en muchos de sus productos, el 100% del algodón " +
                        "proviene de fuentes sostenibles, y se ha comprometido a utilizar poliéster 100% reciclado para el 2024. La firma efectúa auditorías en casi toda " +
                        "su cadena de suministros para garantizar que se cumplen los estándares laborales. " +
                        "Además, se compromete a reducir el carbono y el desperdicio de agua en toda su cadena de producción. ";
                int imageView11 = R.drawable.yeezy;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });

        cardView33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 6/10";
                String favorites = "Favorites: 152";
                String downloads = "Downloads: 68";
                String productTitle = "Nike";
                String productName = "Nike Air Force 1";
                String productDesc = "Style handcraft";
                String productPrice = "104,99";
                String productLongText = "ALZA EL VUELO A TODA POTENCIA.\n" +
                        "La leyenda sigue viva en las zapatillas Nike Air Force 1, fieles a sus orígenes con su icónico estilo AF1 y la tecnología " +
                        "Nike Air para disfrutar de durabilidad y comodidad en todo momento.\n" +
                        "\n" +
                        "Ventajas:\n" +
                        "Parte superior de piel para ofrecer durabilidad y comodidad.\n" +
                        "Unidad Air-Sole encapsulada para ofrecer una amortiguación excepcional.\n" +
                        "Suela exterior de goma para proporcionar durabilidad y tracción.";
                int imageView11 = R.drawable.airforce;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });


//-----------------------------------------------------------------------------------------------

        cardView41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 5/10";
                String favorites = "Favorites: 40";
                String downloads = "Downloads: 16";
                String productTitle = "Converse";
                String productName = "Converse Chuck Taylor All Star Platform High";
                String productDesc = "Style handcraft";
                String productPrice = "84,99$";
                String productLongText = "Creadas en 1917, las sneakers Chuck Taylor All Star son las zapatillas de baloncesto originales. " +
                        "Su uso ha cambiado a lo largo de los años, pero siguen siendo perfectas en su simplicidad. Con su silueta atemporal," +
                        " la suela de caucho vulcanizado y el parche del tobillo inconfundible, las Chuck están listas para que las vivas a tu manera.";
                int imageView11 = R.drawable.converse;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });

        cardView42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 9/10";
                String favorites = "Favorites: 280";
                String downloads = "Downloads: 145";
                String productTitle = "Nike";
                String productName = "Nike Tuned 1";
                String productDesc = "Style handcraft";
                String productPrice = "174,99$";
                String productLongText = "ADOPTA TU GRANDEZA.\n" +
                        "\n" +
                        "Las Nike Air Max Plus van en busca de la grandeza. Estos icónicos colores de negro y oro muestran el estilo del campeonato " +
                        "para ofrecer un look victorioso. La experiencia Tuned Air ofrece estabilidad y sujeción premium cuando entras con valentía en el futuro.\n" +
                        "\n" +
                        "Ventajas:\n" +
                        "Detalles metalizados llamativos, como la etiqueta con logotipo Swoosh, el propio logotipo Swoosh y la presilla de la lengüeta para un " +
                        "look \"Bad Boy\" en esta potente unidad Air Max.\n" +
                        "Unidades Max Air destacadas para proporcionar una amortiguación ligera y mayor estabilidad y sujeción.\n" +
                        "Parte superior de malla y piel sintética para una mayor comodidad, transpirabilidad y durabilidad.\n" +
                        "Estructura de plástico en la parte superior que enmarca el pie y aporta un estilo clásico.\n" +
                        "\n" +
                        "Detalles del producto:\n" +
                        "Elementos reflectantes en la lengüeta\n" +
                        "Este producto no está diseñado para su uso como equipo de protección individual (EPI)";
                int imageView11 = R.drawable.tuned;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });

        cardView43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rating = "Rating: 6/10";
                String favorites = "Favorites: 152";
                String downloads = "Downloads: 68";
                String productTitle = "Jordan";
                String productName = "Jordan 1 Mid";
                String productDesc = "Style handcraft";
                String productPrice = "129,99$";
                String productLongText = "Esta nueva iteración del AJ1 Retro High OG combina " +
                        "el diseño tradicional de Jordan con dos de los colorways más codiciados en el mundo de las sneakers: el 'Baroque Brown' " +
                        "y el 'Racer Pink'." +
                        "\n" +
                        "Su atractivo look, inspirado en el vertiginoso ritmo de vida de Tokio, está logrado con un upper en capas de gamuza de lujo, " +
                        "una paleta de colores innovadores y elementos insignias clásicos, como la etiqueta tejida de la lengüeta y el Swoosh metalizado del costado." +
                        "\n" +
                        "Además, este par está acondicionada con una unidad Air bajo el talón que proporciona la misma amortiguación ligera que disfrutaba Michael " +
                        "Jordan mientras jugaba.";
                int imageView11 = R.drawable.jordan_mid;
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(RATING, rating);
                intent.putExtra(FAVORITES, favorites);
                intent.putExtra(DOWNLOADS, downloads);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);
                intent.putExtra(PRODUCT_IMAGE, imageView11);
                intent.putExtra(PRODUCT_LONG_TEXT, productLongText);
                startActivity(intent);
            }
        });



    }
}