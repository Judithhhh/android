package com.example.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DavidBowieAdapter extends ArrayAdapter<DavidBowie> {
    public DavidBowieAdapter(Context context, ArrayList<DavidBowie> davidBowieArrayList) {
        super(context, 0, davidBowieArrayList);
    }

    //getView
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        /*
        if (convertView == null) {
            //inflarem el converView
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_row, parent, false);
        }
        //hooks
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView textView = convertView.findViewById(R.id.textView);

        DavidBowie actualDavidaBowie = getItem(position);
        if (actualDavidaBowie != null) {
            imageView.setImageResource(actualDavidaBowie.getImgCover());
            textView.setText(actualDavidaBowie.getCoverName());
        }
        return convertView;
        }*/

        return init(position,convertView, parent);
    }

    //covertView --> reutiliza el spinner_row tantas entradas de discos tengas

    //gwtDropDownView
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        /*
        if (convertView == null) {
            //inflarem el converView
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_row, parent, false);
        }
        //hooks
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView textView = convertView.findViewById(R.id.textView);

        DavidBowie actualDavidaBowie = getItem(position);
        if (actualDavidaBowie != null) {
            imageView.setImageResource(actualDavidaBowie.getImgCover());
            textView.setText(actualDavidaBowie.getCoverName());
        }
        return convertView;
        }*/
        return init(position,convertView, parent);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    //Manera de no repetir:
    //hacer el init y despues pasarlo a cada uno, porque el codigo es el mismo :)

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            //inflarem el converView
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_row, parent, false);
        }
        //hooks
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView textView = convertView.findViewById(R.id.textView);

        DavidBowie actualDavidaBowie = getItem(position);
        if (actualDavidaBowie != null) {
            imageView.setImageResource(actualDavidaBowie.getImgCover());
            textView.setText(actualDavidaBowie.getCoverName());
        }
        return convertView;
    }







}
