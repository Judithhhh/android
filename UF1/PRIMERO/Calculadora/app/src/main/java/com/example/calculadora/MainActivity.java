package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView textResult;
    private EditText num1;
    private EditText num2;
    private Button btAdd;
    private Button btSubst;
    private Button btMult;
    private Button btDiv;
    private Button btPower;
    private Button btCe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook = assinacions/enllaços

        textResult = (TextView) findViewById(R.id.textResult);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        btAdd = (Button) findViewById(R.id.btAdd);
        btSubst = (Button) findViewById(R.id.btSubst);
        btMult = (Button) findViewById(R.id.btMult);
        btDiv = (Button) findViewById(R.id.btDiv);
        btPower = (Button) findViewById(R.id.btPower);
        btCe = (Button) findViewById(R.id.btCe);

        textResult.setText("0.0");

        //ADD
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double resultAdd = Double.parseDouble(num1.getText().toString().trim())
                            + Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultAdd + "");
                }
            }
        });

        //SUBST
        btSubst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double resultSubst = Double.parseDouble(num1.getText().toString().trim())
                            - Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultSubst + "");
                }
            }
        });

        //MULT
        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(num1.getText().toString().trim().isEmpty() ||
                num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double resultMult = Double.parseDouble(num1.getText().toString().trim())
                        * Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultMult + "");
                }
            }
        });

        //DIV
        btDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast toast = Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER, 30, 280);
                    toast.show();
                } else if (num2.getText().toString().trim().equals("0") ||
                        num2.getText().toString().trim().equals("0.0")) {
                    Toast.makeText(MainActivity.this, "Divide by zero", Toast.LENGTH_SHORT).show();
                } else {
                    double resultDiv = Double.parseDouble(num1.getText().toString().trim())
                            / Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(resultDiv + "");
                }
            }
        });

        //POWER
        btPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double resultPower = Math.pow(Double.parseDouble(num1.getText().toString().trim()),
                        Double.parseDouble(num2.getText().toString().trim()));
                    textResult.setText(resultPower + "");
                }
            }
        });

        //CE
        btCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    String resultCe = "0.0";
                    textResult.setText(resultCe + "");
                }
            }
        });

    }
}