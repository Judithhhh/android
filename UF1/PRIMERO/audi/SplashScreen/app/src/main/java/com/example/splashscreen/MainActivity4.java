package com.example.splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.net.ssl.HandshakeCompletedEvent;

public class MainActivity4 extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView;
    public static final int SPLASH_SCREEN = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        //Hooks
        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(imageView, "app_name");
                pairs[1] = new Pair<View, String>(textView, "textTitle");

                Intent intent = new Intent(MainActivity4.this, DashboardActivity4.class);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                            MainActivity4.this, pairs
                    );

                    startActivity(intent, options.toBundle());
                }

            }
        };

        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, SPLASH_SCREEN);


    }
}