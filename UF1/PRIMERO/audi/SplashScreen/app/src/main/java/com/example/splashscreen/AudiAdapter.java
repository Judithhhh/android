package com.example.splashscreen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class AudiAdapter extends ArrayAdapter<Audi> {
    public AudiAdapter(Context context, ArrayList<Audi> AudiArrayList) {
        super(context, 0, AudiArrayList);
    }

    //getView
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position,convertView, parent);
    }

    //covertView --> reutiliza el spinner_row tantas entradas de discos tengas

    //gwtDropDownView
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position,convertView, parent);
    }

    //Manera de no repetir:
    //hacer el init y despues pasarlo a cada uno, porque el codigo es el mismo :)

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            //inflarem el convertView
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_row, parent, false);
        }
        //hooks
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView textView = convertView.findViewById(R.id.textView);

        Audi actualAudi = getItem(position);
        if (actualAudi != null) {
            imageView.setImageResource(actualAudi.getImgCover());
            textView.setText(actualAudi.getCoverName());
        }
        return convertView;
    }
}