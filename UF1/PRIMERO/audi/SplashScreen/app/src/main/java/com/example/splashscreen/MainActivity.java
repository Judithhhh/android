package com.example.splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private Spinner spinnerAudi;
    private ArrayList<Audi> mAudi;
    private AudiAdapter mAdapter;

    private TextView already;

    DatePickerDialog picker;
    private EditText textDate;

    private Switch switchNewsletter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        spinnerAudi = (Spinner) findViewById(R.id.spinnerAudi);

        already = (TextView) findViewById(R.id.already);

        already.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MainActivity.this, DashboardActivity4.class);
                startActivity(intent);
            }
        });


        textDate = (EditText) findViewById(R.id.textDate);

        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Select actual date
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                //DataPickerDialog
                //DataPickerDialog.OnDateSetListener()

                picker = new DatePickerDialog(MainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {
                    @Override
                        //mostrar la data seleccionada
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        textDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                        }, year, month, day);
                picker.show();
            }
        });


        //instanciar el Audi
        mAudi = new ArrayList<>();
        mAudi.add(new Audi("Audi A3 Sportback ", R.drawable.audi_a3));
        mAudi.add(new Audi("Audi A1 Sportback - Adrenalin edition", R.drawable.audi_a1_azul));
        mAudi.add(new Audi("Audi A1 Sportback - Black line edition", R.drawable.audi_a1_rojo));
        mAudi.add(new Audi("Audi A7 Sportback", R.drawable.audi_a7));
        mAudi.add(new Audi("Audi A8", R.drawable.audi_a8));
        mAudi.add(new Audi("Audi Q3 - S line", R.drawable.audi_q3));
        mAudi.add(new Audi("Audi e-tron Sportback", R.drawable.audi_etron));


        //SpinnerAudi
        mAdapter = new AudiAdapter(this, mAudi);
        spinnerAudi.setAdapter(mAdapter);
        spinnerAudi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Audi actualAudi = (Audi) parent.getItemAtPosition(position);
                String coverName = actualAudi.getCoverName();
                Toast.makeText(MainActivity.this, coverName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Toast
            }
        });

        switchNewsletter = (Switch) findViewById(R.id.switchNewsletter);
        if (switchNewsletter != null) {
            switchNewsletter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this, "" + isChecked,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "" + isChecked,
                            Toast.LENGTH_SHORT).show();
                }
            }
            });
        }



    }
}