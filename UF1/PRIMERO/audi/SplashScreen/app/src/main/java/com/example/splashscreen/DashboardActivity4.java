package com.example.splashscreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DashboardActivity4 extends AppCompatActivity {

    private TextView ForgotPassword;
    private TextView newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard4);

        ForgotPassword = (TextView) findViewById(R.id.ForgotPassword);

        ForgotPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                Intent intent = new Intent (DashboardActivity4.this, MainActivity2.class);
                startActivity(intent);
            }
        });


        newUser = (TextView) findViewById(R.id.newUser);

        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (DashboardActivity4.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}