package com.example.reciclerviewclass;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<Instagram> nInstagram;
    Context nContext;

    public MyAdapter(ArrayList<Instagram> nInstagram, Context nContext) {
        this.nInstagram = nInstagram;
        this.nContext = nContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(nContext);
        View view = inflater.inflate(R.layout.row_layout, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        Picasso.get().load(nInstagram.get(position).getUrlImage()).fit().centerCrop().into(holder.imageView);

        holder.textName.setText(nInstagram.get(position).getName());

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(nContext, DetailsActivity.class);
                intent.putExtra("name", nInstagram.get(position).getName());
                intent.putExtra("desc", nInstagram.get(position).getDesc());

                //Intent de la imagen
                intent.putExtra("urlimg", nInstagram.get(position).getUrlImage());

                nContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return nInstagram.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textName;

        ConstraintLayout rowLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textName = itemView.findViewById(R.id.textName);

            rowLayout = itemView.findViewById(R.id.mainLayout);
        }
    }


}
