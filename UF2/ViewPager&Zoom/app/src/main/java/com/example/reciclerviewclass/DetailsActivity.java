package com.example.reciclerviewclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    private TextView textName;
    private TextView textDesc;
    private ImageView imageDetail;


    private int[] mImagesIds = new int[] {
            R.drawable.callenuevayork, R.drawable.coche1,
            R.drawable.coche2, R.drawable.edificios,
            R.drawable.empierestate, R.drawable.frambuesas,
            R.drawable.lago, R.drawable.mar2, R.drawable.piedras};

    String name, urlImage, desc;
    int myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


       /*PhotoView photoView = findViewById(R.id.photo_view);
        photoView.setImageResource(R.drawable.callenuevayork);
        photoView.setImageResource(R.drawable.coche1);
        photoView.setImageResource(R.drawable.coche2);
        photoView.setImageResource(R.drawable.edificios);
        photoView.setImageResource(R.drawable.empierestate);
        photoView.setImageResource(R.drawable.frambuesas);
        photoView.setImageResource(R.drawable.lago);
        photoView.setImageResource(R.drawable.mar2);
        photoView.setImageResource(R.drawable.piedras);*/



        textName = findViewById(R.id.textName);
        textDesc = findViewById(R.id.textDesc);
        imageDetail = findViewById(R.id.imgDetailsActivity);

        String name, urlImage, desc;

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);


        PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);

        ImageAdapter imageAdapter = new ImageAdapter(this, mImagesIds);
        viewPager.setAdapter(imageAdapter);
        //photoView.setImageResource(imageAdapter);


        getData();
        setData();
    }


    private void getData(){

        name = getIntent().getStringExtra("name");
        desc = getIntent().getStringExtra("desc");
        urlImage = getIntent().getStringExtra("urlimg");

    }

    private void setData() {
        textName.setText(name);
        textDesc.setText(desc);

        Picasso.get().load(urlImage)
                .fit()
                .centerCrop()
                .into(imageDetail);
    }



}