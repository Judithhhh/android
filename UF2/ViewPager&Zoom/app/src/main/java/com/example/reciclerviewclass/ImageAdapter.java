package com.example.reciclerviewclass;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;


public class ImageAdapter extends PagerAdapter {

    private Context mContext;
    private int[] mImagesIds;

    ImageAdapter(Context context, int[] imagesIds) {
        mContext = context;
        mImagesIds = imagesIds;
    }


    @Override
    public int getCount() {
        return mImagesIds.length;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //return super.instantiateItem(container, position);
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(mImagesIds[position]);
        container.addView(imageView, 0);
        return imageView;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        //return false;
        return view == object;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((ImageView) object);
    }

}

