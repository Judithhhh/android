package com.example.animaciongif;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity3 extends AppCompatActivity {

    ImageView imageView;

    AnimationDrawable animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        imageView = findViewById(R.id.imageView);

        //siempre:
        animation = new AnimationDrawable();


        //añadir con frames:
        /*
        animation.addFrame(getResources().getDrawable(R.drawable.rocket17), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket18), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket19), 40);
        ... se haria con todas las fotos xd
        */

        //con el for:
        String sImage;
        for (int i = 1; i <= 225; i++) {
            sImage = "spaceship";
            animation.addFrame(
                    getResources().getDrawable(
                            getResources().getIdentifier(sImage + i,"drawable",
                                    getPackageName())),20);
        }

        animation.setOneShot(true);
        imageView.setImageDrawable(animation);
        animation.start();

    }
}