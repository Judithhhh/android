package com.example.animaciongif;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    ImageView imageView;
    TextView textTitle;

    AnimationDrawable animation;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        imageView = findViewById(R.id.imageView);
        textTitle = findViewById(R.id.textTitle);
        textTitle.setText("Star");
        //siempre:
        animation = new AnimationDrawable();


        //añadir con frames:
        /*
        animation.addFrame(getResources().getDrawable(R.drawable.rocket17), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket18), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket19), 40);
        ... se haria con todas las fotos xd
        */

        //con el for:
        String sImage;
        for (int i = 17; i <= 167; i++) {
            sImage = "rocket";
            animation.addFrame(
                    getResources().getDrawable(
                            getResources().getIdentifier(sImage + i,"drawable",
                                   getPackageName())),20);
        }

        animation.setOneShot(true);
        imageView.setImageDrawable(animation);
        animation.start();


        objectAnimator1 = ObjectAnimator.ofFloat(textTitle,"scaleX", 0f, 1f);
        objectAnimator2 = ObjectAnimator.ofFloat(textTitle,"scaleY", 0f, 1f);
        objectAnimator3 = ObjectAnimator.ofFloat(textTitle,"alpha", 1f, 0f);
        objectAnimator4 = ObjectAnimator.ofFloat(textTitle,"alpha", 0f, 1f);

        objectAnimator1.setStartDelay(2000);
        objectAnimator2.setStartDelay(2000);
        objectAnimator3.setStartDelay(0);
        objectAnimator4.setStartDelay(2000);

        objectAnimator1.setDuration(500);
        objectAnimator2.setDuration(500);
        objectAnimator3.setDuration(0);
        objectAnimator4.setDuration(0);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
        animatorSet.start();

        textTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity3.class);
                startActivity(intent);
            }
        });
    }

}