package com.example.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    private String title[];
    private String desc[];
    private int images[];
    private Context context;

    public MyAdapter(String[] title, String[] desc, int[] images, Context context) {
        this.title = title;
        this.desc = desc;
        this.images = images;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.myTextTitleRow.setText(title[position]);
        holder.myTextDescRow.setText(desc[position]);
        holder.myImageRow.setImageResource(images[position]);
    }

    @Override
    public int getItemCount() {
        return title.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView myTextTitleRow;
        TextView myTextDescRow;
        ImageView myImageRow;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myTextTitleRow = itemView.findViewById(R.id.textTitleRow);
            myTextDescRow = itemView.findViewById(R.id.textDescRow);
            myImageRow = itemView.findViewById(R.id.imageRow);
        }
    }


}


