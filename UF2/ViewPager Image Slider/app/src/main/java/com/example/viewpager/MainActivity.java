package com.example.viewpager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    private int[] mImagesIds= new int[] {
            R.drawable.coloradopiedras,
            R.drawable.lago,
            R.drawable.mar1,
            R.drawable.mar2};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);

        //Crear ImageAdapter
        ImageAdapter imageAdapter = new ImageAdapter(this, mImagesIds);
        viewPager.setAdapter(imageAdapter);
    }
}