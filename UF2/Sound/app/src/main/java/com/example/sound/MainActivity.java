package com.example.sound;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    SeekBar seekVolume;
    SeekBar seekSound;
    Button antBt;
    Button nextBt;
    Button playBt;
    Button pauseBt;
    MediaPlayer mediaPlayer;
    public static int REWIND_TIME = 5000;
    public static int FASTFORWARD_TIME = 5000;
    public static int UPDATE_SEEKBAR = 100;
    Handler handler = new Handler(Looper.getMainLooper());
    AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekVolume = (SeekBar) findViewById(R.id.seekVolume);
        seekSound = (SeekBar) findViewById(R.id.seekSound);
        antBt = (Button) findViewById(R.id.antBt);
        nextBt = (Button) findViewById(R.id.nextBt);
        playBt = (Button) findViewById(R.id.playBt);
        pauseBt = (Button) findViewById(R.id.pauseBt);
        mediaPlayer = MediaPlayer.create(this, R.raw.sound);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        playBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudio();
            }
        });

        pauseBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseAudio();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                seekSound.setProgress(0);
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
                Toast.makeText(MainActivity.this, "Song has Finished", Toast.LENGTH_SHORT).show();
            }
        });

        antBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewind();
            }
        });

        nextBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fastforward();
            }
        });

        seekSound.setMax(mediaPlayer.getDuration());
        seekSound.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser){
                    mediaPlayer.seekTo(progress);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekVolume.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekVolume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @Override
    protected void onStop() {
        releaseMediaPlayer();
        super.onStop();
    }

    public void playAudio (){
        if (mediaPlayer!= null){
            mediaPlayer.start();
        }else {
            mediaPlayer = MediaPlayer.create(this, R.raw.sound);
            mediaPlayer.start();
        }
        UpdateSeekBarProgres updateSeekBarProgres = new UpdateSeekBarProgres();
        handler.post(updateSeekBarProgres);
    }

    public void pauseAudio (){
        mediaPlayer.pause();
    }

    public void releaseMediaPlayer (){
        if (mediaPlayer != null){
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void rewind(){
        int currentPosition = mediaPlayer.getCurrentPosition();
        if (currentPosition - REWIND_TIME > 0){
            mediaPlayer.seekTo(currentPosition-REWIND_TIME);
        }
    }

    public void fastforward(){
        int currentPosition = mediaPlayer.getCurrentPosition();
        int duretion = mediaPlayer.getDuration();
        if (currentPosition + FASTFORWARD_TIME < duretion){
            mediaPlayer.seekTo(currentPosition + FASTFORWARD_TIME);
        }
    }

    public class UpdateSeekBarProgres implements Runnable{

        @Override
        public void run() {
            if (mediaPlayer != null){
                seekSound.setProgress(mediaPlayer.getCurrentPosition());
                handler.postDelayed(this, UPDATE_SEEKBAR);
            }
        }
    }
}

