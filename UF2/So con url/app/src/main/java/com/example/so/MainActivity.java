package com.example.so;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private Button play;
    private Button pause;
    private Button last;
    private Button next;
    private TextView volume;
    private TextView progress;
    private SeekBar skVolume;
    private SeekBar skProgress;
    String url = "https://docs.google.com/uc?export=download&id=1x8HzkICgpY5F8QhGOrlme_OIVVUgbb5H";
    MediaPlayer mediaPlayer;
    AudioManager audioManager;
    public static int REWIND_TIME = 5000;
    public static int FASTFORWARD_TIME = 5000;
    public static int UPDATE_SEEKBAR = 100;

    public void playAudio() {

        mediaPlayer.start();
        UpdateSeekBarProgress updateSeekBarProgress = new UpdateSeekBarProgress();
        handler.post(updateSeekBarProgress);
    }


    public void pauseAudio() {
        mediaPlayer.pause();
    }

    @Override
    protected void onStop() {
        releaseMediaPlayer();
        super.onStop();
    }

    private void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private void rewind() {
        int currentPosition = mediaPlayer.getCurrentPosition();
        if (currentPosition - REWIND_TIME > 0) {
            mediaPlayer.seekTo(currentPosition - REWIND_TIME);
        }
    }

    private void fastForward() {
        int currentPosition = mediaPlayer.getCurrentPosition();
        int duration = mediaPlayer.getDuration();
        if (currentPosition + FASTFORWARD_TIME < duration) {
            mediaPlayer.seekTo(currentPosition + FASTFORWARD_TIME);
        }
    }

    Handler handler = new Handler(Looper.getMainLooper());

    public class UpdateSeekBarProgress implements Runnable {
        @Override
        public void run() {
            if (mediaPlayer != null) {
                skProgress.setProgress(mediaPlayer.getCurrentPosition());
                handler.postDelayed(this, UPDATE_SEEKBAR);
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play = findViewById(R.id.play);
        pause = findViewById(R.id.pause);
        last = findViewById(R.id.anterior);
        next = findViewById(R.id.seguent);
        volume = findViewById(R.id.volumeText);
        progress = findViewById(R.id.progresText);
        skVolume = findViewById(R.id.skVolume);
        skProgress = findViewById(R.id.skProgress);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC
        );
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
        } catch
        (IllegalArgumentException e) {
            e.printStackTrace();
        } catch
        (IllegalStateException e) {
            e.printStackTrace();
        } catch
        (IOException e) {
            e.printStackTrace();
        }

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void
            onClick(View v) {
                playAudio();
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void
            onClick(View v) {
                pauseAudio();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void
            onCompletion(MediaPlayer mp) {
                skProgress.setProgress(0);
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
                Toast.makeText(MainActivity.this, "Song has finished", Toast.LENGTH_SHORT).show();
            }
        });

        last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void
            onClick(View v) {
                rewind();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fastForward();


            }
        });

        skProgress.setMax(mediaPlayer.getDuration());
    }
}