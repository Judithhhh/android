package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

        private TextView textTitle;
        private TextView textDesc;
        private ImageView image;

        String title;
        String desc;
        int myImage;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);

            textTitle = (TextView) findViewById(R.id.textTitle);
            textDesc = (TextView) findViewById(R.id.textDesc);
            image = (ImageView) findViewById(R.id.image);


            getData();
            setData();
        }

    private void setData() {
            textTitle.setText(title);
            textDesc.setText(desc);
            image.setImageResource(myImage);
        }

    private void getData() {

            if (getIntent().hasExtra("image") &&
                getIntent().hasExtra("title") &&
                getIntent().hasExtra("desc")) {

                title = getIntent().getStringExtra("title");
                desc = getIntent().getStringExtra("desc");
                myImage = getIntent().getIntExtra("image", 1);

            } else {
                Toast.makeText(this, "No data to display.", Toast.LENGTH_SHORT).show();
            }

        }

}
