package com.example.action;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonWeb;
    private Button  buttonMap;
    private Button  buttonInstagram;
    private Button  buttonFacebook;
    private Button  buttonEmail;
    private Button  buttonViewWeb;
    private Button  buttonCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonWeb = findViewById(R.id.buttonWeb);
        buttonMap = findViewById(R.id. buttonMap);
        buttonInstagram = findViewById(R.id. buttonInstagram);
        buttonFacebook = findViewById(R.id. buttonFacebook);
        buttonEmail = findViewById(R.id. buttonEmail);
        buttonViewWeb = findViewById(R.id. buttonViewWeb);
        buttonCall = findViewById(R.id. buttonCall);

        //------------------------------------------------------------------------------------------

        buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://play.google.com/store/apps/dev?id=4734916851270416020&gl=ES";
                //url a adobe
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            }
        });

        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri location = Uri.parse("geo:0,0?q=1600+Escola+del+Treball,+Barcelona,+Espanya");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                startActivity(mapIntent);
            }
        });

        buttonInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("instagram://instagram");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //Si no troba la app Instagram, obrirà internet
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.instagram.com/instagram/?hl=es")));
                }
            }
        });

        buttonFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlId ="fb://page/proactivaservice";
                String urlUrl ="https://www.facebook.com/proactivaservice";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlId)));
                }catch (ActivityNotFoundException e){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlUrl)));
                }
            }
        });


        buttonEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");

                String[] TO = {"user1@escoladeltreball.org", "user2@escoladeltreball.org"};
                String[] CC = {"address1@gmail.com", "address2@gmail.com"};

                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject of the mail");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "This is the text to send with the mail");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MainActivity.this, "There is no email client installed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonViewWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Web.class);
                startActivity(intent);
            }
        });


        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityCall.class);
                startActivity(intent);
            }
        });


    }
}