package com.example.reciclerviewclass;

public class Instagram {

    private String urlImage;
    private String name;
    private String desc;


    public Instagram(String urlImage, String name, String desc) {
        this.urlImage = urlImage;
        this.name = name;
        this.desc = desc;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
