package com.example.reciclerviewclass;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    private TextView textName;
    private TextView textDesc;
    private ImageView imageDetail;

    String name, urlImage, desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        textName = findViewById(R.id.textName);
        textDesc = findViewById(R.id.textDesc);
        imageDetail = findViewById(R.id.imgDetailsActivity);

        String name, urlImage, desc;

        getData();
        setData();
    }

    private void getData(){

    name = getIntent().getStringExtra("name");
    desc = getIntent().getStringExtra("desc");
    urlImage = getIntent().getStringExtra("urlimg");

    }

    private void setData() {
        textName.setText(name);
        textDesc.setText(desc);

        Picasso.get().load(urlImage)
                .fit()
                .centerCrop()
                .into(imageDetail);
    }




}