package com.example.reciclerviewclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    ArrayList<Instagram> arrayllista = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();
    }

    private void initData() {

        Instagram uno1 = new Instagram(
                "https://i.picsum.photos/id/1029/4887/2759.jpg?hmac=uMSExsgG8_PWwP9he9Y0LQ4bFDLlij7voa9lU9KMXDE",
                "Foto 1",
                "Descripcion 1"
                );

        Instagram uno2 = new Instagram(
                "https://i.picsum.photos/id/1043/5184/3456.jpg?hmac=wsz2e0aFKEI0ij7mauIr2nFz2pzC8xNlgDHWHYi9qbc",
                "Foto 2",
                "Descripcion 2"
                );

        Instagram uno3 = new Instagram(
                "https://i.picsum.photos/id/1048/5616/3744.jpg?hmac=N5TZKe4gtmf4hU8xRs-zbS4diYiO009Jni7n50609zk",
                "Foto 3",
                "Descripcion 3"
                );

        Instagram uno4 = new Instagram(
                "https://i.picsum.photos/id/1062/5092/3395.jpg?hmac=o9m7qeU51uOLfXvepXcTrk2ZPiSBJEkiiOp-Qvxja-k",
                "Foto 4",
                "Descripcion 4"
                );

        Instagram uno5 = new Instagram(
                "https://i.picsum.photos/id/1067/5760/3840.jpg?hmac=gO_V7rUFdM8YddyLysCQet4CS0CzSvUcfAtHI1ismLM",
                "Foto 5",
                "Descripcion 5"
                );

        Instagram uno6 = new Instagram(
                "https://i.picsum.photos/id/1071/3000/1996.jpg?hmac=rPo94Qr1Ffb657k6R7c9Zmfgs4wc4c1mNFz7ND23KnQ",
                "Foto 6",
                "Descripcion 6"
        );

        Instagram uno7 = new Instagram(
                "https://i.picsum.photos/id/376/5450/3623.jpg?hmac=SqetbGmid_0u1jDln90xGDYNCHDOpwshsaTY-t6Wly8",
                "Foto 7",
                "Descripcion 7"
        );

        Instagram uno8 = new Instagram(
                "https://i.picsum.photos/id/231/4088/2715.jpg?hmac=PxhkmiNJrVS5AgI8U-r_IsWSN5a7cTjpjIbvmtpLMDI",
                "Foto 8",
                "Descripcion 8"
        );

        Instagram uno9 = new Instagram(
                "https://i.picsum.photos/id/237/3500/2095.jpg?hmac=y2n_cflHFKpQwLOL1SSCtVDqL8NmOnBzEW7LYKZ-z_o",
                "Foto 9",
                "Descripcion 9"
        );

        Instagram uno10 = new Instagram(
                "https://i.picsum.photos/id/182/2896/1944.jpg?hmac=lzw4TC7qF2R3BEJu05d0M6rdglY57ugjugCP6XoiMbQ",
                "Foto 10",
                "Descripcion 10"
        );

        Instagram uno11 = new Instagram(
                "https://i.picsum.photos/id/192/2352/2352.jpg?hmac=jN5UExysObV7_BrOYLdxoDKzm_c_lRM6QkaInKT_1Go",
                "Foto 11",
                "Descripcion 11"
        );

        Instagram uno12 = new Instagram(
                "https://i.picsum.photos/id/249/3000/2000.jpg?hmac=U2oOCXdXUwt6ftKMC4icF6eS8dGDUZJM_0-qi0JEqjk",
                "Foto 12",
                "Descripcion 12"
        );

        Instagram uno13 = new Instagram(
                "https://i.picsum.photos/id/281/4928/3264.jpg?hmac=0XHN8kT-vNOM5Sl2vfxmJk0uWAbHSj5EUMAO9eQ0XFM",
                "Foto 13",
                "Descripcion 13"
        );

        Instagram uno14 = new Instagram(
                "https://i.picsum.photos/id/133/2742/1828.jpg?hmac=0X5o8bHUICkOIvZHtykCRL50Bjn1N8w1AvkenF7n93E",
                "Foto 14",
                "Descripcion 14"
        );

        Instagram uno15 = new Instagram(
                "https://i.picsum.photos/id/145/4288/2848.jpg?hmac=UkhcwQUE-vRBFXzDN1trCwWigpm7MXG5Bl5Ji103QG4",
                "Foto 15",
                "Descripcion 15"
        );

        Instagram uno16 = new Instagram(
                "https://i.picsum.photos/id/158/4836/3224.jpg?hmac=Gu_3j3HxZgR74iw1sV0wcwlnSZSeCi7zDWLcjblOp_c",
                "Foto 16",
                "Descripcion 16"
        );

        Instagram uno17 = new Instagram(
                "https://i.picsum.photos/id/177/2515/1830.jpg?hmac=G8-2Q3-YPB2TreOK-4ofcmS-z5F6chIA0GHYAe5yzDY",
                "Foto 17",
                "Descripcion 17"
        );

        Instagram uno18 = new Instagram(
                "https://i.picsum.photos/id/238/2560/1440.jpg?hmac=wKo4dLHwDntZmO_fdtnKtsnmRcPMACca3m5J5vx2AVc",
                "Foto 18",
                "Descripcion 18"
        );

        Instagram uno19 = new Instagram(
                "https://i.picsum.photos/id/299/5498/3615.jpg?hmac=NqSpUzSsD7mgU34riF8xDMZkcSQY345kONcjjOqqjBA",
                "Foto 19",
                "Descripcion 19"
        );

        Instagram uno20 = new Instagram(
                "https://i.picsum.photos/id/304/6016/4000.jpg?hmac=ULWayz92dKMWLEZviIJkgLRd6J5FClakkDq_3VmXbw0",
                "Foto 20",
                "Descripcion 20"
        );

        Instagram uno21 = new Instagram(
                "https://i.picsum.photos/id/336/1600/1060.jpg?hmac=V0ApxMul9odmMOXCPn6z_NHFnFT_-SmbQfpzughJahU",
                "Foto 21",
                "Descripcion 21"
        );

        Instagram uno22 = new Instagram(
                "https://i.picsum.photos/id/392/5580/3720.jpg?hmac=SHJx3DtQS1aAPntmf27wp2mQ5FqQHFKh7hai6wdZSrA",
                "Foto 22",
                "Descripcion 22"
        );

        Instagram uno23 = new Instagram(
                "https://i.picsum.photos/id/513/4373/3280.jpg?hmac=LkZSEFr5H-jsaqmKTdANAlVWv6Zb38bDJxz5jQEyU0g",
                "Foto 23",
                "Descripcion 23"
        );

        Instagram uno24 = new Instagram(
                "https://i.picsum.photos/id/453/2048/1365.jpg?hmac=A8uxtdn4Y600Z5b2ngnn9hCXAx8sUnOVzprtDnz6DK8",
                "Foto 24",
                "Descripcion 24"
        );

        Instagram uno25 = new Instagram(
                "https://i.picsum.photos/id/429/4128/2322.jpg?hmac=_mAS4ToWrJBx29qI2YNbOQ9IyOevQr01DEuCbArqthc",
                "Foto 25",
                "Descripcion 25"
        );

        Instagram uno26 = new Instagram(
                "https://i.picsum.photos/id/442/1909/1262.jpg?hmac=x2Y_bIJcLoz2hP7l1F8uZcrAC0eMggHKccKwVgXW7mM",
                "Foto 26",
                "Descripcion 26"
        );

        Instagram uno27 = new Instagram(
                "https://i.picsum.photos/id/433/4752/3168.jpg?hmac=Og-twcmaH_j-JNExl5FsJk1pFA7o3-F0qeOblQiJm4s",
                "Foto 27",
                "Descripcion 27"
        );

        Instagram uno28 = new Instagram(
                "https://i.picsum.photos/id/43/1280/831.jpg?hmac=glK-rQ0ppFClW-lvjk9FqEWKog07XkOxJf6Xg_cU9LI",
                "Foto 28",
                "Descripcion 28"
        );

        Instagram uno29 = new Instagram(
                "https://i.picsum.photos/id/506/4561/3421.jpg?hmac=O_h-GpoGREO7aB6sNCFry89dEU9xbkiqs4ykLSGpXjw",
                "Foto 29",
                "Descripcion 29"
        );

        Instagram uno30 = new Instagram(
                "https://i.picsum.photos/id/488/1772/1181.jpg?hmac=psl3qLyDefO6AYqU4TJQ8PNCjS8RdPiP_vRLB8WPVjY",
                "Foto 30",
                "Descripcion 30"
        );

        arrayllista.add(uno1);
        arrayllista.add(uno2);
        arrayllista.add(uno3);
        arrayllista.add(uno4);
        arrayllista.add(uno5);
        arrayllista.add(uno6);
        arrayllista.add(uno7);
        arrayllista.add(uno8);
        arrayllista.add(uno9);
        arrayllista.add(uno10);
        arrayllista.add(uno11);
        arrayllista.add(uno12);
        arrayllista.add(uno13);
        arrayllista.add(uno14);
        arrayllista.add(uno15);
        arrayllista.add(uno16);
        arrayllista.add(uno17);
        arrayllista.add(uno18);
        arrayllista.add(uno19);
        arrayllista.add(uno20);
        arrayllista.add(uno21);
        arrayllista.add(uno22);
        arrayllista.add(uno23);
        arrayllista.add(uno24);
        arrayllista.add(uno26);
        arrayllista.add(uno27);
        arrayllista.add(uno28);
        arrayllista.add(uno29);
        arrayllista.add(uno30);

        //Hook
        recyclerView = findViewById(R.id.recycleView);

        MyAdapter myAdapter = new MyAdapter(arrayllista, this);
        recyclerView.setAdapter(myAdapter);
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // GRIDLAYOUT --->
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false));
    }
}