package com.example.recyclerviewd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    String title[];
    String desc[];

    int images[] = {R.drawable.guitar, R.drawable.nachos02, R.drawable.mexican_hat03,
                    R.drawable.cactus04, R.drawable.skull05, R.drawable.chili06,
                    R.drawable.mexico07, R.drawable.maracas08, R.drawable.avocado09};

    int[][] mImagesIds = {{R.drawable.aircraft, R.drawable.bicycle},
                        {R.drawable.boat, R.drawable.cable_car_cabin},
                        {R.drawable.camper, R.drawable.car},
                        {R.drawable.convertible, R.drawable.helicopter},
                        {R.drawable.hot_air_balloon, R.drawable.jet_ski},
                        {R.drawable.kayak, R.drawable.limousine},
                        {R.drawable.motorbike, R.drawable.sailboat},
                        {R.drawable.scooter, R.drawable.segway},
                        {R.drawable.sled, R.drawable.submarine}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = getResources().getStringArray(R.array.title);
        desc = getResources().getStringArray(R.array.description);

        //Hook
        recyclerView = findViewById(R.id.recycleView);

        MyAdapter myAdapter = new MyAdapter(title, desc, images, this, mImagesIds);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // --> GRIDLAYOUT recyclerView.setLayoutManager(new GridLayoutManager(this, 2 , LinearLayoutManager.VERTICAL, false));
    }
}