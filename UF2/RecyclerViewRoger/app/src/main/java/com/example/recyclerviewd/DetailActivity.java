package com.example.recyclerviewd;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.viewpager2.widget.ViewPager2;
import androidx.viewpager.widget.ViewPager;

public class DetailActivity extends AppCompatActivity {

    private TextView textTitle;
    private TextView textDesc;
    private ImageView image;
    private int[] mImagesIds;
    ViewPager viewPager;

    String title;
    String desc;
    int myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        textTitle = findViewById(R.id.textTitle);
        textDesc = findViewById(R.id.textDesc);
        image = findViewById(R.id.imageDetail);
        viewPager = findViewById(R.id.viewPager);

        getData();
        setData();
    }

    private void setData() {
        textTitle.setText(title);
        textDesc.setText(desc);
        image.setImageResource(myImage);

        ImageAdapter imageAdapter = new ImageAdapter(this, mImagesIds);
        viewPager.setAdapter(imageAdapter);

    }

    private void getData() {

        if (
                getIntent().hasExtra("title") &&
                getIntent().hasExtra("desc") &&
                getIntent().hasExtra("image")) {

            title = getIntent().getStringExtra("title");
            desc = getIntent().getStringExtra("desc");
            myImage = getIntent().getIntExtra("image", 1);
            mImagesIds = getIntent().getIntArrayExtra("mImagesIds");

        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }
}