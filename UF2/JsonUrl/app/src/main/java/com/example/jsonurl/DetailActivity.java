package com.example.jsonurl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView textName;
    private TextView textCapital;
    private TextView textContinente;
    private ImageView imageView;
    private TextView textDescripcion;

    String name, capital, continente, url, descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textName = findViewById(R.id.textName);
        textCapital = findViewById(R.id.textCapital);
        textContinente = findViewById(R.id.textContinente);
        imageView = findViewById(R.id.imageView);
        textDescripcion = findViewById(R.id.textDescripcion);

        getData();
        setData();
    }

    private void getData() {
        if (getIntent().hasExtra("name") &&
                getIntent().hasExtra("capital")) {
            name = getIntent().getStringExtra("name");
            capital = getIntent().getStringExtra("capital");
            continente = getIntent().getStringExtra("continente");
            url = getIntent().getStringExtra("url");
            descripcion = getIntent().getStringExtra("descripcion");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        textName.setText(name);
        textCapital.setText(capital);
        textContinente.setText(continente);
        textDescripcion.setText(descripcion);
        Picasso.get().load(url)
                .fit()
                .centerCrop()
                .into(imageView);
    }




}
