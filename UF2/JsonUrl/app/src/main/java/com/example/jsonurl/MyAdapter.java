package com.example.jsonurl;

import androidx.collection.CircularArray;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;

import com.squareup.picasso.Picasso;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context mContext;
    private List<Peak> mPeak;

    public MyAdapter(Context mContext, List<Peak> mPeak) {
        this.mContext = mContext;
        this.mPeak = mPeak;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.peak_layout, parent, false);
        return new MyViewHolder(view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView capital;
        ImageView url;
        TextView continente;

        //part 2
        ConstraintLayout peakLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textName);
            capital = itemView.findViewById(R.id.textCapital);
            url = itemView.findViewById(R.id.imageUrl);
            continente = itemView.findViewById(R.id.textContinente);
            //part 2
            peakLayout = itemView.findViewById(R.id.peakLayout);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.name.setText(mPeak.get(position).getName());
        holder.capital.setText(mPeak.get(position).getCapital());
        holder.continente.setText(mPeak.get(position).getContinente());
        Picasso.get().load(mPeak.get(position).getUrl())
                .fit()
                .centerCrop()
                .into(holder.url);

        //part2
        holder.peakLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        //part2
        holder.peakLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("name", mPeak.get(position).getName());
                intent.putExtra("capital", mPeak.get(position).getCapital());
                intent.putExtra("continente", mPeak.get(position).getContinente());
                intent.putExtra("descripcion", mPeak.get(position).getDescripcion());
                intent.putExtra("url", mPeak.get(position).getUrl());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPeak.size();
    }



}
