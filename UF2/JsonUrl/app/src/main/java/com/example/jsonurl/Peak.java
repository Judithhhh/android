package com.example.jsonurl;

public class Peak {
    String name;
    String capital;
    String continente;
    String url;
    String descripcion;

    public Peak(String name, String capital, String continente, String url, String descripcion) {
        this.name = name;
        this.capital = capital;
        this.continente = continente;
        this.url = url;
        this.descripcion = descripcion;
    }

    public Peak() {
    }

    //getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}