package com.example.jsonurl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List peaks = new ArrayList<>();
    private static String JSON_URL =
            "https://run.mocky.io/v3/fd587635-4a88-4405-92e0-54b8aef8b536";
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyAdapter myAdapter = new MyAdapter(this, peaks);
        recyclerView.setAdapter(myAdapter);
    }

    private void getPeaks() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,     //Metode GET o PUT
                JSON_URL,               //url JSON file
                (String) null,          //A JSONObject to post with the request type POST.
                //Null indicates no parameters will be posted
                //along with request.
                new Response.Listener<JSONArray>() { //Listener to receive the JSON response
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject peakObject = response.getJSONObject(i);
                                Peak peak = new Peak(); //empty constructor
                                peak.setName(peakObject.getString("name"));
                                peak.setCapital(peakObject.getString("capital"));
                                peak.setContinente(peakObject.getString("contiente"));
                                peak.setUrl(peakObject.getString("url"));
                                peak.setDescripcion(peakObject.getString("descripcion"));
                                peaks.add(peak);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        recyclerView.setLayoutManager(new
                                LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), peaks);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() { //Error listener, or null to ignore errors
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }

                }
        );
        queue.add(jsonArrayRequest);
    }
}